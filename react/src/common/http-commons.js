import axios from "axios";

export const JSON_CONTENT_TYPE = "application/json"
export const JSON_API_CONTENT_TYPE = "application/vnd.api+json"
const REST_BASE_URL = "http://127.0.0.1:8000/rest/cap/"

/**
 * HTTP REST api access
 *   contentType: accepted json/json api content type
 */
export const httpAccess = (contentType) => {
    return axios.create({
      baseURL: REST_BASE_URL,
      headers: {
        "Content-type": contentType
      }
    });
}

  
  