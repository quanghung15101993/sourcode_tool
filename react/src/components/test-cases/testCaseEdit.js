import {Button, Form, Input, Modal,Select} from "antd";
import React, {useState} from "react";
import Space from "antd/lib/space";
import {openPop,getTestCase,updateTestCase} from "../../store/actions/testCaseAction"
import {connect} from "react-redux";

function TestCaseEdit({testCaseState,testCaseId,openPop,updateTestCase,getTestCase}) {

    const [form] = Form.useForm();
    //The isModalVisibleEdit can't move on the store because it will happend error when user click open Popup 
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    form.resetFields();
    form.setFieldsValue({
        name: testCaseState.name, 
        docTypeId: testCaseState.docTypeId,
        docTypeName: testCaseState.docTypeName,
        captivaId: testCaseState.captivaId,
    });
    const onOk = () => {
        form.submit();
    };

    const handleCancel = () => {
        setIsModalVisibleEdit(false)
    };
     const showModal = () => {
         //This openPop function only fill data auto into the Textbox
        openPop(testCaseId.id)
        //The Popup will arealy open when run to here 
        setIsModalVisibleEdit(true)
    };
   
    const onFinish = async (dataUpdate) =>{   
        let body = JSON.stringify({
            "data": {
                "type": "testCase",
                "id": testCaseId.id,
                "attributes": {
                    "name": dataUpdate.name,
                    "doc_type_id": dataUpdate.docTypeId,
                    "captiva_id": dataUpdate.captivaId,
                }
            }
        })
        await updateTestCase(body,testCaseId.id)
        setIsModalVisibleEdit(false)
        //Change the records in the Table after user Edit 
            getTestCase()
    };

        return (
            <>
                <Space>
                    <Button type="primary" onClick={showModal}>
                    Edit
                </Button>

                <Modal title="Edit testCase"
                       visible={isModalVisibleEdit}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={testCaseState.loading} onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={(values)=>onFinish(values)}>
                        <Form.Item
                            name="name"
                            label="testCase Name:"
                            rules={[{required: true, message: 'Please input testCase Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="docTypeId"
                            label="Document Type Id">
                            <Select
                                placeholder="Select a Project for recog profile"
                                allowClear>
                                {testCaseState.listDocTypeId}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="captivaId"
                            label="Captiva Id">
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
                </Space>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state, props) => ({
    testCaseState: state.testCase,
    testCaseId:props
});

export default connect(mapStateToProps, {openPop,getTestCase,updateTestCase}) (TestCaseEdit);

