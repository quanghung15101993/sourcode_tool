import React, {useEffect,} from "react";
import "antd/dist/antd.css";
import {Col, Input, Row, Table,Modal,Button, Space} from 'antd';
import ProjectCreate from "./projectCreate"
import ProjectEdit from "./projectEdit"
import {getProject,deleteProject} from "../../store/actions/projectAction"
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";

function Project({projectState,getProject,deleteProject}){
    
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
            sorter: true,
            width: '40%',
        },
        {
            title: "Description",
            dataIndex: "description",
            key: "description",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                <ProjectEdit id ={record.id} key={record.id} />
                <Button danger onClick={() => removeColumn(record.id)} key={record.id}>
                        Remove
                </Button>
                </Space>
            ),

        },
    ];

   

    const onSearch = value => {
        getProject({
            search: value,
        })
    }

    //useEffect
    useEffect(() => {
        getProject({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        getProject(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };    

    const removeColumn =  (id) => {
        confirm({
            title: 'Are you sure delete project?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            async onOk() {
              await deleteProject(id)
              //Change the records after Delete 
              getProject()
                
            },
            onCancel() {
            },
        });
    };

    return (
        
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton  /></Col>
                        <Col span={12}><ProjectCreate value="create" key="create" align="end"/></Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="projectTable"
                           columns={columns}
                           dataSource={projectState.dataSource}
                           loading={projectState.loading}
                           pagination={{total: projectState.total, current: projectState.current}}
                           onChange={handleTableChange}
                    />
                </div>
            </div>


        </>

    );
}
//Get state from Store
const mapStateToProps = (state) => ({
    projectState: state.project,
});
//Connect to store
export default connect(mapStateToProps, {getProject,deleteProject})(Project);
