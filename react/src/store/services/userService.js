import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";

const authenticate = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/users/authenticate", data);
};

const getAll = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/users/${id}`);
};


const UserService = {
    authenticate,
    getAll
};

export default UserService;