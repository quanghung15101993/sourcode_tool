import uvicorn


if __name__ == '__main__':
    uvicorn.run("app.routers.root:app", host="127.0.0.1", port=9003, reload=True)