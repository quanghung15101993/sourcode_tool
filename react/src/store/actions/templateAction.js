import { GET_TEMPLATE,INSERT_TEMPLATE,OPEN_POP_TEMPLATE,CLOSE_POP_TEMPLATE } from "../types";
import {REST_API_TEMPLATE,REST_API_TEMPLATE_V,openNotificationCommon} from "../../common/const"
import TemplateService from "../services/templateService";
import DocTypeService from "../services/docTypeService"
import isNotNullOrUndefined from "../../common/utils";

export const getTemplate = (params = {}) => async (dispatch) => {

   try {
       // get response from api
       const repos = await TemplateService.getAll(params);
       const reposDoctype = await DocTypeService.getAllDoctype();
       
       if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)&&
            isNotNullOrUndefined(reposDoctype) && isNotNullOrUndefined(reposDoctype.data)) {
         
         //data doctypes
         let dataDoctype = reposDoctype.data.data
         console.log(dataDoctype);
         let countData = repos.data;
         let list = [];
         let data = countData.data;
         let lenght = data.length
         let count = countData.meta.pagination.count;
         for (let i = 0; i < lenght; i++) {
             
             list.push({
                 id:data[i].id,
                 name: data[i].attributes.name,
                 docTypeId: data[i].attributes.doc_type_id,
                 docTypeName: data[i].attributes.doc_type_name,
                 captivaId: data[i].attributes.captiva_id,
             });
         }
         dispatch({
           type: GET_TEMPLATE,
           // it can write ...count
           count: count,
           dataDoctype: dataDoctype,
           page: params.page,
           payload: list,
           
       });
     }
   } catch (error) {
    console.log(error);
   }
        
};

export const openPop = (param) => async (dispatch) => {
    try {
        if (param === "create") {

            dispatch({             
                type: OPEN_POP_TEMPLATE,
                value: param
            });   
          }else{
           // get response from api
           const repos = await TemplateService.get(param);
           console.log("action",repos);
           dispatch({
               type: OPEN_POP_TEMPLATE,
               payload: repos,
           });       
        };
        
    } catch (error) {
        console.log(error);
    }
}

export const closePop = () => async (dispatch) => {
    try {
        dispatch({
            type: CLOSE_POP_TEMPLATE,
        });
    } catch (error) {
        console.log(error);
    }
};

//Delete
export const deleteTemplate = (id) => async (dispatch) => {
  
    try {
        const res = await TemplateService.remove(id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
};

// Insert
export  const insertTemplate =(body) => async (dispatch) => {
    try {
        const res = await TemplateService.create(body);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            dispatch({
                type: INSERT_TEMPLATE,
            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
  };

// Update
export const updateTemplate = (body,id) => async (dispatch) => {
    try {
        const res = await TemplateService.update(id,body);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
};
