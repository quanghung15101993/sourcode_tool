import {Button, Form, Input, Modal} from "antd";
import React, {useState} from "react";
import Space from "antd/lib/space";
import {openPop,getProject,updateProject} from "../../store/actions/projectAction"
import {connect} from "react-redux";

function ProjectEdit({projectState,projectId,openPop,updateProject,getProject}) {

    const [form] = Form.useForm();
    //The isModalVisibleEdit can't move on the store because it will happend error when user click open Popup 
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    form.resetFields();
    form.setFieldsValue({name: projectState.name, description: projectState.description});
    const onOk = () => {
        form.submit();
    };

    const handleCancel = () => {
        setIsModalVisibleEdit(false)
    };
     const showModal = () => {
         //This openPop function only fill data auto into the Textbox
        openPop(projectId.id)
        //The Popup will arealy open when run to here 
        setIsModalVisibleEdit(true)
    };
   
    const onFinish = async (dataUpdate) => {   
        let body = JSON.stringify({
            "data": {
                "type": "Project",
                "id": projectId.id,
                "attributes": {
                    "name": dataUpdate.name,
                    "description": dataUpdate.description,
                }
            }
        })
       await updateProject(body,projectId.id)
        setIsModalVisibleEdit(false)
        //Change the records in the Table after user Edit 
        getProject()

    };

        return (
            <>
                <Space>
                    <Button type="primary" onClick={showModal}>
                    Edit
                </Button>

                <Modal title="Edit project"
                       visible={isModalVisibleEdit}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={projectState.loading} onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={(values)=>onFinish(values)}>
                        <Form.Item
                            name="name"
                            label="Project Name:"
                            rules={[{required: true, message: 'Please input Project Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="description"
                            label="Description">
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
                </Space>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state, props) => ({
    projectState: state.project,
    projectId:props
});

export default connect(mapStateToProps, {openPop,getProject,updateProject}) (ProjectEdit);

