import React, {useEffect} from "react";
import "antd/dist/antd.css";
import Space from "antd/lib/space";
import {Button, Col, Input, Modal, Row, Table} from 'antd';
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";
import {deleteData, docTypeList} from "../../store/actions/docTypeAction";
import DocTypeForm from "./docTypeForm";
import DocTypeDetail from "./docTypeDetail";

const DocType = ({docTypeState, docTypeList, deleteData}) => {
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "No.",
            dataIndex: "id",
            key: "id",
        },
        {
            title: "Doc Type Name",
            dataIndex: "name",
            key: "name",
            sorter: true,
        },
        {
            title: "Recognition Profile ID",
            dataIndex: "recog_profile_id",
            key: "recog_profile_id",
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                    <DocTypeDetail id={record.id}/>
                    <DocTypeForm id={record.id}/>
                    <Button danger onClick={() => removeColumn(record.id)} key={record.id}>
                        Remove
                    </Button>
                </Space>
            ),

        },
    ];
    const onSearch = (value) => {
        console.log(value);
        docTypeList({
            search: value,
        })
    }

    //useEffect
    useEffect(() => {
        docTypeList({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        docTypeList(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };

    const removeColumn = (id) => {
        confirm({
            title: 'Are you sure delete Doc Type?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteData({id: id});
            },
            onCancel() {
            },
        });
    };

    return (
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton/></Col>
                        <Col span={12}>
                            <DocTypeForm id="" key="create"/>
                        </Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="docTypeTable"
                           columns={columns}
                           dataSource={docTypeState.listDocType}
                           pagination={{total: docTypeState.total, current: docTypeState.current}}
                           onChange={handleTableChange}
                    />
                </div>
            </div>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    docTypeState: state.docTypeReducer,
});
//Connect to store
export default connect(mapStateToProps, {docTypeList, deleteData})(DocType);
