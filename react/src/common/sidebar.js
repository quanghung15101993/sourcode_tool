import React from "react";
import {Link} from "react-router-dom";
import {routes} from "./const";

function Sidebar() {
    return (
        <>
            <ul className="navbar-nav bg-gradient-menu sidebar sidebar-dark accordion" id="accordionSidebar">
                {/* Sidebar - Brand */}
                <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/">
                    <div>
                        <img className="" src="/img/mb.png" style={{width: 70}} alt=""></img>
                    </div>
                    <div className="sidebar-brand-text mx-3">Captiva Tools</div>
                </Link>
                {/* Divider */}
                <hr className="sidebar-divider my-0"></hr>
                {routes.map((route, i) => (
                    <li className="nav-item" key={route.name}>
                        <Link className="nav-link" to={route.path}>
                            <i className={route.icon} style={{fontSize:18}}></i>
                            <span>{route.name}</span>
                        </Link>
                    </li>
                ))}
                {/* Divider */}
                <hr className="sidebar-divider d-none d-md-block"></hr>
                {/* Sidebar Toggler (Sidebar) */}
                <div className="text-center d-none d-md-inline">
                    <button className="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
            </ul>
        </>
    )
}

export default Sidebar;