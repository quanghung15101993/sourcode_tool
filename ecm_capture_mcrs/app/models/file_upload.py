from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field
from .batch import BatchModel

'''
    File upload db model
'''


class FileUploadModel(BaseModel):
    file_name: str = Field(...)
    file_path: Optional[str] = Field(...)
    status: Optional[bool] = Field(...)
    upload_date: Optional[str] = Field(default=datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
    batch: Optional[BatchModel] = None


'''
    file upload request model
'''


class FileUploadRequestModel(BaseModel):
    file_name: str = Field(title="File name")
    file_data: str
    upload_by: Optional[str] = None

    class Config:
        schema_extra = {
            "example": {
                "file_name": "file_1.tiff",
                "file_data": "base64",
                "upload_by": "optional_username"
            }
        }
