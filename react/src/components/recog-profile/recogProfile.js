import React, {useEffect} from "react";
import "antd/dist/antd.css";
import Space from "antd/lib/space";
import {Button, Col, Input, Modal, Row, Table} from 'antd';
import RecogProfileForm from "./recogProfileForm"
import {connect} from "react-redux";
import {deleteRecogProfile, recogProfileList} from "../../store/actions/recogProfileAction";
import isNotNullOrUndefined from "../../common/utils";

const RecogProfile = ({recogState, recogProfileList, deleteRecogProfile}) => {
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
            sorter: true,
            width: '40%',
        },
        {
            title: "Project ID",
            dataIndex: "project_id",
            key: "project_id",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                    <RecogProfileForm id={record.id} key={record.id}/>
                    <Button danger onClick={() => removeColumn(record.id)} key={record.id}>
                        Remove
                    </Button>
                </Space>
            ),

        },
    ];
    const onSearch = (value) => {
        console.log(value);
        recogProfileList({
            search : value,
        })
    }

    //useEffect
    useEffect(() => {
        recogProfileList({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        recogProfileList(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };

    const removeColumn = (id) => {
        confirm({
            title: 'Are you sure delete Recognition Profile?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteRecogProfile({id: id});
            },
            onCancel() {
            },
        });
    };

    return (
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton/></Col>
                        <Col span={12}>
                            <RecogProfileForm id="" key="create"/>
                        </Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="recogProfileTable"
                           columns={columns}
                           dataSource={recogState.listRecogProfile}
                           pagination={{total: recogState.total, current: recogState.current}}
                           onChange={handleTableChange}
                    />
                </div>
            </div>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    recogState: state.recogProfileReducer,
});
//Connect to store
export default connect(mapStateToProps, {recogProfileList, deleteRecogProfile})(RecogProfile);
