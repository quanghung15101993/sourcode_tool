﻿from fastapi import APIRouter, Body
from starlette import status

from app.core.base.response_object import ResponseObject
from app.core.middlewares.exception_handler import error_handle_common
from app.dependencies.file_upload_depends import get_batch_by_id_dp, create_file_upload_result, get_upload_result_srv
from app.dependencies.s3_storage_depends import S3Storage
from app.models.file_upload import FileUploadRequestModel
from app.core.config.logger import logging

batch_upload_router = APIRouter()

'''
    batch upload file endpoint
'''


@batch_upload_router.put("/batchs/{batch_id}/upload",
                         status_code=status.HTTP_200_OK,
                         name="batchs:upload file")
async def create_batch_file_upload(batch_id: str, file_upload: FileUploadRequestModel = Body(...)):
    response = ResponseObject()
    try:

        # verify batch by id
        batch = await get_batch_by_id_dp(batch_id)
        if not batch:
            return response.error(code=status.HTTP_500_INTERNAL_SERVER_ERROR, message="Batch id not found!!")

        # upload file to s3 storage
        upload_status = True
        s3_client = S3Storage()
        s3_file_path = s3_client.upload_file(file_data=file_upload.file_data, file_name=file_upload.file_name,
                                             batch_id=batch_id, bucket_name="capture_bucket")
        # if upload false
        if s3_file_path is None:
            s3_file_path = ""
            upload_status = False
        # create file upload result.
        await create_file_upload_result(file_upload=file_upload, s3_file_path=s3_file_path, batch_id=batch_id,
                                        status=upload_status)
    except Exception as e:
        logging.error(e.__str__())
        return await error_handle_common(exc=e)
    return response.success(data={"message": "OK"})


'''
    Get upload result (success|error) of batch
'''


@batch_upload_router.get("/batchs/{batch_id}/upload-finish",
                         status_code=status.HTTP_200_OK,
                         name="batchs:upload file")
async def get_uploads_result_by_batch(batch_id: str):
    response = ResponseObject()
    try:
        # verify batch by id
        batch = await get_batch_by_id_dp(batch_id)
        if not batch:
            return response.error(code=status.HTTP_500_INTERNAL_SERVER_ERROR, message="Batch id not found!!")
        result = await get_upload_result_srv(batch_id)
        return response.success(result)
    except Exception as e:
        logging.error("Router - get upload result by batch error !! --- message: {msg}".format(msg=e.__str__()))
        return await error_handle_common(exc=e)
