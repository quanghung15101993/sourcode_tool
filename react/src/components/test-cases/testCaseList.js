import React, {useEffect} from "react";
import "antd/dist/antd.css";
import Space from "antd/lib/space";
import {Button, Col, Input, Modal, Row, Table} from 'antd';
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";
import {getTestCaseList} from "../../store/actions/testCaseAction";
import {Link} from "react-router-dom";
import IpFlowForm from "../ip-flow/ipFlowForm";
import TestCaseDetail from "./testCaseDetail";

const TestCaseList = ({testCaseState, getTestCaseList}) => {
    const {Search} = Input;

    const removeColumn = (id) => {
    };

    const columns = [
        {
            title: "No.",
            dataIndex: "tc_id",
            key: "tc_id",
        },
        {
            title: "Project Name",
            dataIndex: "project_name",
            key: "project_name",
            sorter: true,
        },
        {
            title: "Recog Profile Name",
            dataIndex: "recog_profile_name",
            key: "recog_profile_name",
            sorter: true,
        },
        {
            title: "Doctype Name",
            dataIndex: "doctype_name",
            key: "doctype_name",
            sorter: true,
        },
        {
            title: "Template Name",
            dataIndex: "template_name",
            key: "template_name",
            sorter: true,
        },
        {
            title: "Image Result Name",
            dataIndex: "image_result_name",
            key: "image_result_name",
            sorter: true,
        },
        {
            title: "Captiva Model Id",
            dataIndex: "captiva_model_id",
            key: "captiva_model_id",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '10%',
            render: (text, record) => (
                <Space>
                    <TestCaseDetail id={record.tc_id} key={record.tc_id}/>
                    <Button danger onClick={() => removeColumn(record.tc_id)} key={record.tc_id}>
                        Remove
                    </Button>
                </Space>
            ),

        },
    ];
    const onSearch = (value) => {
        getTestCaseList({
            search: value,
        })
    }

    //useEffect
    useEffect(() => {
        getTestCaseList({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        getTestCaseList(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };

    return (
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton/></Col>
                        <Col span={12}>
                            <Link to={'/testcaseCreate'}>
                                <Button type="primary" align="end" style={{"float": "right"}}>Create Test Case</Button>
                            </Link>
                        </Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="recogProfileTable"
                           columns={columns}
                           pagination={{total: testCaseState.total, current: testCaseState.current}}
                           onChange={handleTableChange}
                           expandable={{
                               expandedRowRender: record => <p style={{ margin: 0 }}>Description: {record.tc_description}</p>,
                               rowExpandable: record => record.tc_description !== null,
                           }}
                           dataSource={testCaseState.listTestCase}
                    />
                </div>
            </div>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});
//Connect to store
export default connect(mapStateToProps, {getTestCaseList})(TestCaseList);
