import React from "react";
import Project from "../components/project/project";
import RecogProfile from "../components/recog-profile/recogProfile";
import CMT from "../components/cmt/cmt";
import IpProfileList from "../components/ip-profile/ipProfileList";
import Template from "../components/templates/template";
import IpFlow from "../components/ip-flow/ipFlow";
import Doctype from "../components/doc-type/docType";
import Extraction from "../components/extractions/extraction";
import TestCase from "../components/test-cases/testCase";
import TestCaseList from "../components/test-cases/testCaseList";


function ContentCommon(props) {
    let compo;
    switch (props.id) {
        case "project":
            compo = <Project/>;
            break;
        case "recog-profile":
            compo = <RecogProfile/>;
            break;
        case "cmt":
            compo = <CMT/>;
            break;
        case "ip-profile":
            compo = <IpProfileList/>;
            break;
        case "ip-flow":
            compo = <IpFlow/>;
            break;
        case "doc-type":
            compo = <Doctype/>;
            break;
        case "extractions":
            compo = <Extraction/>;
            break;
        case "templates":
            compo = <Template/>;
            break;
        case "testcase":
            compo = <TestCaseList/>;
            break;
        case "testcaseCreate":
            compo = <TestCase/>;
            break;
        default:
            compo = <Project/>;
            break;
    }
    return (

        <div className="card shadow">
            {compo}
        </div>
    );
}

export default ContentCommon;
