import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllIpProfile,
  removeIpProfile,
} from "../../store/actions/ipProfileAction";
import { Table, Button, Modal } from "antd";
import { Col, Input, Row } from "antd";
import IpProfileAddForm from "./ipProfileAddForm";
import IpProfileEditForm from "./ipProfileEditForm";
import Space from "antd/lib/space";
import { openNotificationCommon } from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";


const IpProfileList = () => {
  // init components
  const { confirm } = Modal;
  const { Search } = Input;
  const ipProfiles = useSelector((state) => state.stateIpProfile);
  const dispatch = useDispatch();


  // init table columns
  const columns = [
    {
      title: "No.",
      dataIndex: "id",
      key: "id",
      width: "20%",
    },
    {
      title: "Profile Name",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Action",
      key: "action",
      fixed: "right",
      width: "20%",
      render: (record) => (
        <Space>
          <IpProfileEditForm value={record} key={record.id} align="end" />
          <Button
            danger
            onClick={() => removeIpProfileAction(record.id)}
            key={record.id}
          >
            Remove
          </Button>
        </Space>
      ),
    },
  ];

  // init screen method
  useEffect(() => {
    dispatch(getAllIpProfile());
  }, []);

  /**
   * Table handle listener
   *
   * @param {*} pagination
   * @param {*} filters
   * @param {*} sorter
   */
  const tableHandleListener = (pagination, filters, sorter) => {
    let sort = "";
    if (isNotNullOrUndefined(sorter.field)) {
      sort = sorter.order === "ascend" ? sorter.field : "-" + sorter.field;
    }
    dispatch(
      getAllIpProfile({
        page: pagination.current,
        sort: sort,
      })
    );
  };

  /**
   * Search listener
   *
   * @param {*} searchVal
   */
  const onSearchListener = (searchVal) => {
    dispatch(
      getAllIpProfile({
        search: searchVal,
      })
    );
  };

  /**
   * Remove ip profile action
   *
   * @param {*} id
   */
  const removeIpProfileAction = (id) => {
    confirm({
      title: "Are you sure remove IP Profile ?",
      content: "",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        dispatch(removeIpProfile(id)).then(() => {
          // reload table
          dispatch(getAllIpProfile()).then(() => {
            openNotificationCommon(
              "success",
              "Remove IP Profile",
              "Remove IP Profile success!!"
            );
          });
        });
      },
      onCancel() {},
    });
  };

  /**
   *  UI RENDER
   */
  return (
    <div className="card mb-4">
      <div className="card-header py-3">
        <Row>
          <Col span={12}>
            <Search
              placeholder="input search text"
              onSearch={onSearchListener}
              enterButton
            />
          </Col>
          <Col span={12}>
            <Col span={12}>
              <IpProfileAddForm value="create" key="create" align="end" />
            </Col>
          </Col>
        </Row>
      </div>
      <div className="card-body">
        <Table
          rowKey="id"
          columns={columns}
          dataSource={ipProfiles.dataSource.dataList}
          pagination={{
            total: ipProfiles.dataSource.total,
            current: ipProfiles.dataSource.current,
          }}
          onChange={tableHandleListener}
        />
      </div>
    </div>
  );
};

export default IpProfileList;
