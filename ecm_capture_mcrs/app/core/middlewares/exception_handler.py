import enum

from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException, \
    StarletteHTTPException, \
    RequestErrorModel, \
    RequestValidationError
from fastapi.responses import JSONResponse

from app.core.base.response_object import ResponseObject


class ExceptionType(enum.Enum):
    MS_UNAVAILABLE = 500, '990', 'Hệ thống đang bảo trì, quý khách vui lòng thử lại sau'
    MS_INVALID_API_PATH = 500, '991', 'Hệ thống đang bảo trì, quý khách vui lòng thử lại sau'
    DATA_RESPONSE_MALFORMED = 500, '992', 'Có lỗi xảy ra, vui lòng liên hệ admin!'

    def __new__(cls, *args, **kwds):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, http_code, code, message):
        self.http_code = http_code
        self.code = code
        self.message = message


class BusinessException(Exception):
    http_code: int
    code: str
    message: str

    def __init__(self, http_code: int = None, code: str = None, message: str = None):
        self.http_code = http_code if http_code else 500
        self.code = code if code else str(self.http_code)
        self.message = message


async def error_handle_common(exc):
    if exc is (RequestValidationError or RequestErrorModel):
        return JSONResponse(
            status_code=400,
            content=jsonable_encoder(ResponseObject().error(400, get_message_validation(exc)))
        )
    if exc is StarletteHTTPException:
        return JSONResponse(
            status_code=exc.status_code,
            content=jsonable_encoder(ResponseObject().error(exc.status_code, get_message_validation(exc)))
        )
    if exc is HTTPException:
        return JSONResponse(
            status_code=500,
            content=jsonable_encoder(ResponseObject().error(500, "Có lỗi xảy ra, vui lòng liên hệ admin!"))
        )
    return JSONResponse(
        status_code=400,
        content=jsonable_encoder(ResponseObject().error(500, "Có lỗi xảy ra, vui lòng liên hệ admin!"))
    )


def get_message_validation(exc):
    message = ""
    for error in exc.errors():
        message += "/'" + str(error.get("loc")[1]) + "'/" + ': ' + error.get("msg") + ", "

    message = message[:-2]

    return message
