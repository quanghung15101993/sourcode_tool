import {Form, Input} from "antd";
import FormItem from "antd/lib/form/FormItem";
import React from "react";
import $ from "jquery";

import {
    PROCESS_IMAGE_TEST_CASE,
    LOAD_TEMPLATE_TEST_CASE,
    LOAD_RECOG_DOCTYPE_TEST_CASE,
    RUN_TEST_CASE,
    LOAD_PROJECT_TEST_CASE,
    LOAD_PROJECT_FLOW_RECOG_TEST_CASE,
    UPLOAD_IMAGE_TEST_CASE,
    REMOVE_IMAGE_TIFF,
    INSERT_ASSERT_DATA,
    SHOW_IMAGE_TEST_CASE,
    LOAD_FLOW_TEST_CASE,
    LOAD_DOCTYPE_TEMPLATE_TEST_CASE,
    GET_DESCRIP_VALUE, GET_TEST_CASE_ALL_VIEW, GET_TEST_CASE_DETAIL_VIEW
} from "../types";

const initialState = {
// those fields is Form module
    projectFlowId: "",
    flowProjectId: "",
    projectFlowData: [],
    flowProjectData: [],
    testCaseId: "",
    description: "",
// those fields is UploadImage module
    disabledBrowse: true,
    displayUpload: "none",
    disabledProcessImage: "none",
    disabledUpload: true,
    fileList: [],
    browseUpload: [],
    imageInputData: [],
    imageId: "",
    imageTiifCanvas: null,
// those fields is AssertData module
    projecRecogId: "",
    recogData: [],
    recogId: "",
    displayAssertData: "",
    doctypeId: "",
    doctypeName: "",
    doctypeData: [],
    templateData: [],
    templateId: "",
    listData: [],
    singleFields: [],
    listDataResult: [],
    displayAssertDataTag: "none",
    displayRunTestCaseBt: "none",
    dislayAssertDataBt: false,

// get list test case
    listTestCase: [],
    total: 10,
    current: 1,
    // get test case detail
    testCaseDetail: {},
    tableTestCaseDetail: [],
};

const testCaseReducer = (state = initialState, action) => {

    switch (action.type) {
        case GET_TEST_CASE_ALL_VIEW:
            return {
                ...state,
                listTestCase: action.payload,
                current: action.page,
                total: action.count
            };
        case GET_TEST_CASE_DETAIL_VIEW:
            return {
                ...state,
                testCaseDetail: action.payload,
                tableTestCaseDetail: action.listTable,
            };

        case LOAD_PROJECT_TEST_CASE:
            $("#original_img_prev_tif").remove()
            $("#result_img_prev_tif").remove()
            $("#canvas-input-data-tif").remove()
            return {
                ...state,
                projectFlowData: action.data,
                projectRecoData: action.data,
                flowProjectData: [],
                testCaseId: "",
                projecRecogId: "",
                recogData: [],
                recogId: "",
                projectFlowId: "",
                flowProjectId: "",
                // those fields is UploadImage module
                fileList: [],
                browseUpload: [],
                imageInputData: [],
                imageId: "",
                disabledBrowse: true,
                disabledUpload: true,
                displayAssertData:"none",
                doctypeId: "",
                doctypeData: [],
                doctypeName: "",
                templateData: [],
                templateId: "",
                listData: [],
                singleFields: [],
                listDataResult: [],
                displayUpload: "none",
                disabledProcessImage: "none",
            };

        case LOAD_PROJECT_FLOW_RECOG_TEST_CASE:
            return {
                ...state,
                flowProjectData: action.dataFlow,
                recogData: action.dataRecog,
                projectFlowId: action.projectId,
                disabledBrowse: true,
                disabledUpload: true,
                displayUpload: "none",
                // those fields is Form module
                flowProjectId: "",
                testCaseId: "",
                // those fields is UploadImage module
                fileList: [],
                browseUpload: [],
                imageInputData: [],
                imageId: "",
                // those fields is AssertData module
                projecRecogId: "",
                recogId: "",
                displayAssertData:"none",
                doctypeId: "",
                doctypeName: "",
                doctypeData: [],
                templateData: [],
                templateId: "",
                listData: [],
                singleFields: [],
                listDataResult: [],
            };

        case LOAD_FLOW_TEST_CASE:
            return {
                ...state,
                flowProjectId: action.flowId,
                disabledBrowse: false,
                displayUpload: "",
                displayAssertData: "none",
            };
        case PROCESS_IMAGE_TEST_CASE:
            return {
                ...state,
                // : action.tag,
                fileList: [],
                displayAssertData: "",

            };

        case SHOW_IMAGE_TEST_CASE:
            return {
                ...state,
                fileList: action.payload,
                imageInputData: action.payload,
                browseUpload: action.payload,
                listData: [],
                disabledUpload: false
            }
        case UPLOAD_IMAGE_TEST_CASE:
            return {
                ...state,
                imageId: action.imageId,
                testCaseId: action.testCaseId,
                fileList: [],
                disabledUpload: true,
                disabledProcessImage: ""
            }
        case REMOVE_IMAGE_TIFF:
            return {
                ...state,
                fileList: [],
                disabledUpload: true,
                disabledProcessImage: true,
                // those fields is AssertData module
                displayAssertData: "none",
                recogId: "",
                recogData: [],
                doctypeId: "",
                doctypeName: "",
                doctypeData: [],
                templateId: "",
                displayRunTestCaseBt: "none",
                templateData: [],

            }

        case LOAD_TEMPLATE_TEST_CASE:
            var singleFields = action.singleFields
            var listData = []
            singleFields.map((singleField, i) => {
                let data = ""
                Object.keys(singleField).forEach((key) => {
                    if (key === "name") {
                        data = singleField[key];
                    }
                    if (key === "value") {
                        listData.push(<div key={i} className="hung"><Form.Item label={data} name={data}>
                            <Input maxLength={500}/>
                        </Form.Item></div>)
                    }
                })
            })
            return {
                ...state,
                templateId: action.templateId,
                listData: listData,
                singleFields: action.singleFields,
                imageTiifCanvas: action.imageTiifCanvas,
                displayAssertDataTag: ""
            };

        case RUN_TEST_CASE:
            var extractResults = action.extractResult
            var listData = []
            extractResults.map((extractResult, i) => {
                let data = ""
                Object.keys(extractResult).forEach((key) => {
                    if (key === "name") {
                        data = extractResult[key];
                    }
                    if (key === "value") {
                        console.log(key, extractResult[key]);
                        listData.push(<div key={i}><Form.Item label={data}>
                            <Input maxLength={500} value={extractResult[key]}/>
                        </Form.Item></div>)
                    }
                })
            })
            return {
                ...state,
                templateId: action.templateId,
                listDataResult: listData,
                displayAssertDataTag: ""
            };
        case INSERT_ASSERT_DATA:
            return {
                ...state,
                dislayAssertDataBt: true,
                displayRunTestCaseBt: "",
            };

        case LOAD_RECOG_DOCTYPE_TEST_CASE:
            return {
                ...state,
                doctypeData: action.data,
                recogId: action.recogId,
                doctypeId: "",
                doctypeName: "",
                templateData: [],
                templateId: "",
                listData: [],
                listDataResult: [],
                imageTiifCanvas: null,
                singleFields: [],
                displayAssertDataTag: "none",
                displayRunTestCaseBt: "none",
            };
        case LOAD_DOCTYPE_TEMPLATE_TEST_CASE:
            return {
                ...state,
                templateData: action.data,
                doctypeId: action.doctypeId,
                doctypeName: action.doctypeName,
                templateId: "",
                listData: [],
                listDataResult: [],
                imageTiifCanvas: null,
                singleFields: [],
                displayAssertDataTag: "none",
                displayRunTestCaseBt: "none",
                dislayAssertDataBt:false,

            };
        case GET_DESCRIP_VALUE:
            return {
                ...state,
                description: action.value
            };


        default:
            return state;
    }
};

export default testCaseReducer;
