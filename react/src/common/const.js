import Project from "../components/project/project";
import RecogProfile from "../components/recog-profile/recogProfile";
import IpProfileList from "../components/ip-profile/ipProfileList";
import {notification} from "antd";
import CMT from "../components/cmt/cmt";
import IpFlow from "../components/ip-flow/ipFlow";
import Doctype from "../components/doc-type/docType";
import TestCaseList from "../components/test-cases/testCaseList";
/*const routes menu*/
export const routes = [
    {
        path: "/project",
        component: Project,
        name : "Project",
        breadcrumb: "project",
        icon : "fas fa-city",
    },
    {
        path: "/ip-project",
        component: RecogProfile,
        name : "IP Project",
        breadcrumb: "ip-project",
        icon : "fas fa-fw fa-clone",
    },
    {
        path: "/ip-flow",
        component: IpFlow,
        name : "IP Flow",
        breadcrumb: "ip-flow",
        icon : "fas fa-fw fa-clone",
    },
    {
        path: "/ip-profile",
        component: IpProfileList,
        name : "Image Process Profile",
        breadcrumb: "ip-profile",
        icon : "fas fa-fw fa-align-justify",
    },
    {
        path: "/recog-profile",
        component: RecogProfile,
        name : "Recognition Profile",
        breadcrumb: "recog-profile",
        icon : "fas fa-fw fa-clipboard-list",
    },
    {
        path: "/templates",
        component: RecogProfile,
        name : "Templates",
        breadcrumb: "templates",
        icon : "fas fa-fw fa-tachometer-alt",
    },
    {
        path: "/doc-type",
        component: Doctype,
        name : "Doc Type",
        breadcrumb: "doc-type",
        icon : "fas fa-fw fa-list",
    },
    {
        path: "/extractions",
        component: RecogProfile,
        name : "Extraction",
        breadcrumb: "extractions",
        icon : "fas fa-fw fa-list",
    },
    {
        path: "/testcase",
        component: TestCaseList,
        name : "Testcase",
        breadcrumb: "testcase",
        icon : "fas fa-fw fa-calendar-alt",
    },
    {
        path: "/cmt",
        component: CMT,
        name : "CMT",
        breadcrumb: "cmt",
        icon : "fas fa-fw fa-address-card",
    }
];
/*const routes menu*/
const domain = "http://127.0.0.1:8000/rest/cap";
//const domain_ocr_cmt = "http://10.7.16.122:5000";
const domain_ocr_cmt = "http://10.63.130.25:5000";
export const REST_API_REG_PROFILE = domain + "/reg-profiles"
export const REST_API_PROJECT_ALL = domain + "/projects?size=1000"
export const REST_API_PROJECT = domain + "/projects"
//template
export const REST_API_TEMPLATE_V = domain + "/template-v"
export const REST_API_TEMPLATE= domain + "/templates"
export const REST_API_POST_CMT = domain_ocr_cmt + "/ai/ocr/v1.0/get-cic-content"

/*notification common*/
export const openNotificationCommon = (type, message, description) => {
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 3,
    });

    notification[type]({
        message: message,
        description: description,
    });
};
/*notification common*/

export const stopLoading = () => {
    document.getElementById("loading").classList.remove("loading");
}

export const startLoading= () => {
    document.getElementById("loading").classList.add("loading");
}


