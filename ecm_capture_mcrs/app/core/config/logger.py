import logging
import sys

'''The logs are save to logging.conf file'''
logging.basicConfig(
    level=logging.INFO,
    format="{asctime} {levelname:<8} -- module: [{module}] --  message: [{message}]",
    style= '{',
    filename="logging.conf",
    filemode='a'
)

''' Error The logs are save to logging.conf file'''
logging.basicConfig(
    level=logging.ERROR,
    format="{asctime} {levelname:<8} -- module: [{module}] --  message: [{message}]",
    style= '{',
    filename="logging_error.conf",
    filemode='a'
)

'''The logs are run on console '''
root = logging.getLogger()
root.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('[%(asctime)s] - [%(levelname)s] - [%(module)s] -  [%(message)s]')
handler.setFormatter(formatter)
root.addHandler(handler)
