import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const getAllTestCaseV = (params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get("/test-cases-v");
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get("/test-cases-v",request);
};

const getTestCaseV = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/test-cases-d/${id}`);
};

const getAll = (params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get("/test-cases");
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get("/test-cases",request);
};

const get = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/test-cases/${id}`);
};

const create = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/test-cases", data);
};

const update = (id, data) => {
    return httpAccess(JSON_API_CONTENT_TYPE).put(`/test-cases/${id}`, data);
};

const remove = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).delete(`/test-cases/${id}`);
};

const getAllProject = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/projects?size=1000");
};
const getProjectFlow = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/ip-flows-v?filter[project_id]=${id}`);
};

const getProjectRecog = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/reg-profiles?filter[project_id]=${id}`);
};

const uploadImageProcess = (body) => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/image/upload",body);
};

const postImageProcess = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/ip-flows-v?filter[project_id]=${id}`);
};

const imageProcessTestCase = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).post(`/test-cases/${id}/generate-image-result`);
};

const postAsserData = (body) => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/assert-data",body);
};

const getRecogDocType = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/doc-types?filter[recog_profile]=${id}`);
};

const getDocTypeTemplate = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/template-v?filter[doc_type_id]=${id}`);
};

const runTestCase = (id) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/test-cases/${id}/run`);
};


const TestCaseService = {
    getAllTestCaseV,
    getTestCaseV,
    getAll,
    get,
    create,
    update,
    remove,
    getAllProject,
    getProjectFlow,
    uploadImageProcess,
    postImageProcess,
    getProjectRecog,
    imageProcessTestCase,
    getRecogDocType,
    postAsserData,
    runTestCase,
    getDocTypeTemplate
};

export default TestCaseService;