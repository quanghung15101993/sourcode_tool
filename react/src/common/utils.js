

/**
 * Check object is not null or undefined
 * @param {*} object 
 * @returns 
 */
const isNotNullOrUndefined = (object) => {
    if (typeof(object) !== "undefined" && object !== null) {
            return true;
    }
    return false;
}

export default isNotNullOrUndefined;