# Define repository model handle is here
from app.core.utils import datetime_now


def batch_model_helper(batch) -> dict:
    return {
        "id": str(batch['_id']),
        "business_type": batch['business_type'],
        "created_by": batch['created_by'],
        "created_date": batch['created_date']
    }


'''
    File upload model helper
'''


def file_upload_helper(file_upload) -> dict:
    return {
        "id": str(file_upload['_id']) if file_upload['_id'] else "",
        "file_name": file_upload['file_name'],
        "file_path": file_upload['file_path'],
        "status": file_upload['status'],
        "upload_date": file_upload['upload_date'],
        "batch": file_upload['batch'],
    }


'''
    Function helper for create file upload result
'''


def file_upload_result_helper(file_upload, s3_file_path, batch_id, status=False) -> dict:
    return {
        "file_name": file_upload.file_name,
        "file_path": s3_file_path,
        "status": status,
        "batch": batch_id,
        "upload_date": datetime_now()
    }
