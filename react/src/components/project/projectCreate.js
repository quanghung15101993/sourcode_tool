import {Button, Form, Input, Modal} from "antd";
import React from "react";
import {openPop,closePop,insertData,getProject} from "../../store/actions/projectAction"
import {connect} from "react-redux";

function ProjectCreate({projectState,openPop,closePop,insertData,getProject}) {
    
    const [form] = Form.useForm();
    const showModal = (value) => {
        form.resetFields();
        openPop(value)
    };

    const handleCancel = () => {
        closePop()
    };


    const onOk = () => {
        form.submit();
    };

    const onFinish = async (dataInsert) => {   
        console.log(dataInsert);
        let body = JSON.stringify({
            "data": {
                "type": "Project",
                "attributes": {
                    "name": dataInsert.name,
                    "description": dataInsert.description,
                }
            }
        })
        await insertData(body)
        //Change the records in the Table after user Edit 
        getProject()
       
    };
        return (
            <>
                <Button type="primary" onClick={(value)=>showModal("create")} style={{cssFloat:"right"}}>
                    Create project
                </Button>

                <Modal title="Create project"
                       visible={projectState.isModalVisibleCreate}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={projectState.projectState} onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="Project Name:"
                            rules={[{required: true, message: 'Please input project name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="description"
                            label="Description:"
                            >
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state) => ({
    projectState: state.project,
});

export default connect(mapStateToProps, {openPop,closePop,insertData,getProject}) (ProjectCreate);

