import {
    CREATE_UPDATE_IP_FLOW,
    IP_FLOW,
} from "../types";

const initialState = {
    idFlow : "",
    listProject: [],
    listIpFlow: [],
    total: 10,
    current: 1,
};

const IpFlowReducer = (state = initialState, action) => {
    switch (action.type) {
        case IP_FLOW:
            return {
                ...state,
                listIpFlow : action.payload,
                current: action.page,
                total: action.count
            };
        case CREATE_UPDATE_IP_FLOW:
            return {
                ...state,
                idFlow : action.id,
                listProject : action.payload,
            };

        default:
            return state;
    }
};

export default IpFlowReducer;