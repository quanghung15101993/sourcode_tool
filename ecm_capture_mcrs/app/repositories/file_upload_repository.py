from .repositories_helper import file_upload_helper
from ..core.base import base_repository
from ..core.config.logger import logging

file_upload_collections = base_repository.get_collection('file_upload')

'''
    create file upload result.
'''


async def create_file_upload_repo(file_upload_data: dict) -> dict:
    try:
        file_upload = await file_upload_collections.insert_one(file_upload_data)
        logging.info("Create new file upload success -- ID: {id}".format(id=file_upload.inserted_id))
        return file_upload.inserted_id
    except Exception as e:
        logging.error("Create new file upload error! -- message: {msg}".format(msg=e.__str__()))
        raise e


'''
    Get upload result by batch
'''


async def get_upload_result_by_batch_repo(batch_id: str) -> []:
    try:
        upload_results = []
        result_cursor = file_upload_collections.find({"batch": batch_id})
        if not result_cursor:
            return upload_results
        # get list result from cursor
        upload_results = await result_cursor.to_list(length=300)
        logging.info("Get upload result success! ")
        return upload_results
    except Exception as e:
        logging.error("Get upload result error! -- message: {msg}".format(msg=e.__str__()))
        raise e
