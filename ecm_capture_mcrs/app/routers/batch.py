

from fastapi import Body, APIRouter
from fastapi.encoders import jsonable_encoder

from app.core.base.response_object import ResponseObject
from app.models.batch import BatchModel
from app.repositories.batch_repositoy import retrieve_batch, create_batch
from app.core.config.logger import logging

batch_router = APIRouter()


@batch_router.get("/batches", response_description="Batch information retrieved")
async def get_batch():
    logging.info("------start get all batch------")
    response_obj = ResponseObject()
    batches = await retrieve_batch()
    if not batches or len(batches) > 0:
        return response_obj.success(batches)


@batch_router.post("/batch", response_description="Batch data added into the repository")
async def create_batch_data(batch: BatchModel = Body(...)):
    logging.info("------start create batch------")
    response_obj = ResponseObject()
    print(batch)
    batch = jsonable_encoder(batch)
    new_batch = await create_batch(batch)
    return response_obj.success(new_batch)
