from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field

'''
    Batch infomation model
'''


class BatchModel(BaseModel):
    name: str = Field(title="Batch name")
    business_type: str = Field(title="Business document type")
    created_by: str = Field(title="User create batch")
    created_date: Optional[str] = Field(title="Batch create date time",
                                        default=datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))

    class Config:
        schema_extra = {
            "example": {
                "name": "CTQT_User_...",
                "business_type": "CTQT",
                "created_by": "username"
            }
        }
