import {RECOG_PROFILE, CREATE_UPDATE_RECOG_PROFILE} from "../types";
import React from "react";
import {Select} from "antd";
import RecogProfileService from "../services/recogProfileService";
import {openNotificationCommon, startLoading, stopLoading} from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";

const {Option} = Select;

export const recogProfileList = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await RecogProfileService.getAll(params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let count = repos.data.meta.pagination.count;
            let dataList = repos.data.data;
            let list = [];
            for (let i = 0; i < dataList.length; i++) {
                list.push({
                    key : dataList[i].id,
                    id: dataList[i].id,
                    name: dataList[i].attributes.name,
                    project_id: dataList[i].attributes.project_id,
                });
            }
            dispatch({
                type: RECOG_PROFILE,
                count: count,
                page: params.page,
                payload: list,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

export const createUpdateRecogProfile = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await RecogProfileService.getAllProject();
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let dataProject = repos.data.data;
            let list = [];
            for (let i = 0; i < dataProject.length; i++) {
                list.push(
                    <Option value={dataProject[i].id} key={dataProject[i].id}>{dataProject[i].attributes.name}</Option>
                );
            }
            if (params.id == undefined) {
                dispatch({
                    type: CREATE_UPDATE_RECOG_PROFILE,
                    payload: list,
                    id: "",
                })
            } else {
                const dataRecog = await RecogProfileService.get(params.id);
                if (isNotNullOrUndefined(dataRecog) && isNotNullOrUndefined(dataRecog.data)) {
                    let data = dataRecog.data.data.attributes;
                    let projectId = data.project_id + "";
                    params.form.setFieldsValue({
                        name: data.name,
                        projectId: projectId,
                    });
                    dispatch({
                        type: CREATE_UPDATE_RECOG_PROFILE,
                        payload: list,
                        id: params.id,
                    })
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// Insert
export const insertData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "RecogProfile",
                "attributes": {"name": params.name, "project_id": params.projectId}
            }
        });
        const res = await RecogProfileService.create(data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
            // reset table
            const repos = await RecogProfileService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

// Update
export const updateData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "RecogProfile",
                "id": params.id,
                "attributes": {
                    "name": params.name,
                    "project_id": params.projectId
                }
            }
        });
        const res = await RecogProfileService.update(params.id, data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Update bản ghi', 'Cập nhật thành công bản ghi');
            // reset tables
            const repos = await RecogProfileService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Update bản ghi', 'Cập nhật không thành công bản ghi');
    }
    stopLoading();
};

// Delete
export const deleteRecogProfile = (params) => async (dispatch) => {
    startLoading();
    try {
        const res = await RecogProfileService.remove(params.id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
            // reset tables
            const repos = await RecogProfileService.getAll({});
            resetTable(repos, dispatch);
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
    stopLoading();
};

// reset table
const resetTable = (repos, dispatch) => {
    if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
        console.log(repos);
        let count = repos.data.meta.pagination.count;
        let dataList = repos.data.data;
        let list = [];
        for (let i = 0; i < dataList.length; i++) {
            list.push({
                key : dataList[i].id,
                id: dataList[i].id,
                name: dataList[i].attributes.name,
                project_id: dataList[i].attributes.project_id,
            });
        }
        dispatch({
            type: RECOG_PROFILE,
            count: count,
            page: 1,
            payload: list,
        })

    }
};




