import React, {useEffect,} from "react";
import "antd/dist/antd.css";
import {Col, Input, Row, Table,Modal,Button, Space} from 'antd';
import TemplateCreate from "./templateCreate"
import TemplateEdit from "./templateEdit"
import {getTemplate,deleteTemplate} from "../../store/actions/templateAction"
import {connect} from "react-redux";
import { getProject } from "../../store/actions/projectAction";
import isNotNullOrUndefined from "../../common/utils";

function Template({templateState,getTemplate,deleteTemplate}){
    
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
            sorter: true,
            width: '40%',
        },
        {
            title: "Document Type Id",
            dataIndex: "docTypeId",
            key: "docTypeId",
            sorter: true,
        },
        {
            title: "Document Type Name",
            dataIndex: "docTypeName",
            key: "docTypeName",
            sorter: true,
        },
        {
            title: "Captiva Id",
            dataIndex: "captivaId",
            key: "captivaId",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                <TemplateEdit id ={record.id} key={record.id} />
                <Button danger onClick={() => removeColumn(record.id)} key={record.id}>
                        Remove
                </Button>
                </Space>
            ),

        },
    ];
    const onSearch = value =>{
        getTemplate({
            search : value,
        })
    }

    //useEffect
    useEffect(() => {
        getTemplate({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        getTemplate(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };    

    const removeColumn = (id) =>{
        confirm({
            title: 'Are you sure delete project?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            async onOk() {
                 await deleteTemplate(id)                
                //Change the records after Delete 
                getTemplate();
                
            },
            
            onCancel() {
            },
        });
    };

    return (
        
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton  /></Col>
                        <Col span={12}><TemplateCreate value="create" key="create" align="end"/></Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="projectTable"
                           columns={columns}
                           dataSource={templateState.dataSource}
                           loading={templateState.loading}
                           pagination={{total: templateState.total, current: templateState.current}}
                           onChange={handleTableChange}
                    />
                </div>
            </div>


        </>

    );
}
//Get state from Store
const mapStateToProps = (state) => ({
    templateState: state.template,
});
//Connect to store
export default connect(mapStateToProps, {getTemplate,deleteTemplate})(Template);
