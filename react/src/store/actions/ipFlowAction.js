import {IP_FLOW, CREATE_UPDATE_IP_FLOW} from "../types";
import React from "react";
import {Select} from "antd";
import {openNotificationCommon, startLoading, stopLoading} from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";
import IpFlowService from "../services/ipFlowService";

const {Option} = Select;

export const ipFlowList = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from apitestcaseCreate
        const repos = await IpFlowService.getAll(params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let count = repos.data.meta.pagination.count;
            let dataList = repos.data.data;
            let list = [];
            for (let i = 0; i < dataList.length; i++) {
                list.push({
                    key : dataList[i].id,
                    ip_flow_id: dataList[i].id,
                    ip_flow_name: dataList[i].attributes.ip_flow_name,
                    ip_flow_version: dataList[i].attributes.ip_flow_version,
                    is_active: dataList[i].attributes.is_active,
                    project_id: dataList[i].attributes.project_id,
                    project_name: dataList[i].attributes.project_name,
                });
            }
            dispatch({
                type: IP_FLOW,
                count: count,
                page: params.page,
                payload: list,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

export const createUpdateIpFlow = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await IpFlowService.getAllProject();
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let dataProject = repos.data.data;
            let list = [];
            for (let i = 0; i < dataProject.length; i++) {
                list.push(
                    <Option value={dataProject[i].id} key={dataProject[i].id}>{dataProject[i].attributes.name}</Option>
                );
            }
            if (params.id == undefined) {
                dispatch({
                    type: CREATE_UPDATE_IP_FLOW,
                    payload: list,
                    id: "",
                })
            } else {
                const dataRecog = await IpFlowService.get(params.id);
                if (isNotNullOrUndefined(dataRecog) && isNotNullOrUndefined(dataRecog.data)) {
                    let data = dataRecog.data.data.attributes;
                    params.form.setFieldsValue({
                        name: data.ip_flow_name,
                        projectId: data.project_id + "",
                    });
                    dispatch({
                        type: CREATE_UPDATE_IP_FLOW,
                        payload: list,
                        id: params.id,
                    })
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// Insert
export const insertData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "IpFlow",
                "attributes": {"name": params.name, "project_id": params.projectId}
            }
        });
        const res = await IpFlowService.create(data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
            // reset table
            const repos = await IpFlowService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

// Update
export const updateData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "IpFlow",
                "id": params.id,
                "attributes": {
                    "name": params.name,
                    "project_id": params.projectId
                }
            }
        });
        const res = await IpFlowService.update(params.id, data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Update bản ghi', 'Cập nhật thành công bản ghi');
            // reset tables
            const repos = await IpFlowService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Update bản ghi', 'Cập nhật không thành công bản ghi');
    }
    stopLoading();
};

// Delete
export const deleteIpFlow = (params) => async (dispatch) => {
    startLoading();
    try {
        const res = await IpFlowService.remove(params.id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
            // reset tables
            const repos = await IpFlowService.getAll({});
            resetTable(repos, dispatch);
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
    stopLoading();
};

// reset table
const resetTable = (repos, dispatch) => {
    if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
        console.log(repos);
        let count = repos.data.meta.pagination.count;
        let dataList = repos.data.data;
        let list = [];
        for (let i = 0; i < dataList.length; i++) {
            list.push({
                key : dataList[i].id,
                ip_flow_id: dataList[i].id,
                ip_flow_name: dataList[i].attributes.ip_flow_name,
                ip_flow_version: dataList[i].attributes.ip_flow_version,
                is_active: dataList[i].attributes.is_active,
                project_id: dataList[i].attributes.project_id,
                project_name: dataList[i].attributes.project_name,
            });
        }
        dispatch({
            type: IP_FLOW,
            count: count,
            page: 1,
            payload: list,
        })

    }
};




