import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

function FooterCommon() {

    return (
        <Footer style={{ textAlign: 'center' }}>Copyright &copy; Captiva Tools {new Date().getFullYear()}</Footer>
    );
}

export default FooterCommon;
