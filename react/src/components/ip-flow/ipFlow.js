import React, {useEffect} from "react";
import "antd/dist/antd.css";
import Space from "antd/lib/space";
import {Button, Col, Input, Modal, Row, Table} from 'antd';
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";
import IpFlowForm from "./ipFlowForm";
import {deleteIpFlow, ipFlowList} from "../../store/actions/ipFlowAction";
import {Link} from "react-router-dom";

const IpFlow = ({ipFlowState, ipFlowList, deleteIpFlow}) => {
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "No.",
            dataIndex: "ip_flow_id",
            key: "ip_flow_id",
        },
        {
            title: "IP Flow Name",
            dataIndex: "ip_flow_name",
            key: "ip_flow_name",
            sorter: true,
        },
        {
            title: "IP Flow Version",
            dataIndex: "ip_flow_version",
            key: "ip_flow_version",
            sorter: true,
        },
        {
            title: "Is Active",
            dataIndex: "is_active",
            key: "is_active",
            sorter: true,
        },
        {
            title: "Project Id",
            dataIndex: "project_id",
            key: "project_id",
            sorter: true,
        },
        {
            title: "Project Name",
            dataIndex: "project_name",
            key: "project_name",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                    <Link className="nav-link" to={`/ip-flow/step/${record.ip_flow_id}`}>
                        <Button type="primary" key="create">Step</Button>
                    </Link>

                    <IpFlowForm id={record.ip_flow_id} key={record.ip_flow_id}/>
                    <Button danger onClick={() => removeColumn(record.ip_flow_id)} key={record.ip_flow_id}>
                        Remove
                    </Button>
                </Space>
            ),

        },
    ];
    const onSearch = (value) => {
        console.log(value);
        ipFlowList({
            search: value,
        })
    }

    //useEffect
    useEffect(() => {
        ipFlowList({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order == "ascend" ? sorter.field : "-" + sorter.field
        }
        ipFlowList(
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };

    const removeColumn = (id) => {
        confirm({
            title: 'Are you sure delete IP Flow?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteIpFlow({id: id});
            },
            onCancel() {
            },
        });
    };

    return (
        <>
            <div className="card mb-4">
                <div className="card-header py-3">
                    <Row>
                        <Col span={12}><Search placeholder="input search text" onSearch={onSearch} enterButton/></Col>
                        <Col span={12}>
                            <IpFlowForm id="" key="create"/>
                        </Col>
                    </Row>
                </div>
                <div className="card-body">
                    <Table key="recogProfileTable"
                           columns={columns}
                           dataSource={ipFlowState.listIpFlow}
                           pagination={{total: ipFlowState.total, current: ipFlowState.current}}
                           onChange={handleTableChange}
                    />
                </div>
            </div>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    ipFlowState: state.ipFlowReducer,
});
//Connect to store
export default connect(mapStateToProps, {ipFlowList, deleteIpFlow})(IpFlow);
