import {Button, Form, Input, Modal,Select} from "antd";
import React from "react";
import {openPop,closePop,insertTestCase,getTestCase} from "../../store/actions/testCaseAction"
import {connect} from "react-redux";

function TestCaseCreate({testCaseState,openPop,closePop,insertTestCase,getTestCase}) {
    
    const [form] = Form.useForm();
    const showModal = (value) => {
        form.resetFields();
        openPop(value)
    };

    const handleCancel = () => {
        closePop()
    };


    const onOk = () => {
        form.submit();
    };

    const onFinish = async (dataInsert) => {   
        console.log(dataInsert);
        let body = JSON.stringify({
            "data": {
                "type": "testCase",
                "attributes": {
                    "name": dataInsert.name,
                    "description": dataInsert.description,
                }
            }
        })
        await insertTestCase(body)
        //Change the records in the Table after user Edit 
        getTestCase()
    };
        return (
            <>
                <Button type="primary" onClick={(value)=>showModal("create")} style={{cssFloat:"right"}}>
                    Create testCase
                </Button>

                <Modal title="Create testCase"
                       visible={testCaseState.isModalVisibleCreate}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={testCaseState.testCaseState} onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                    <Form.Item
                            name="name"
                            label="testCase Name:"
                            rules={[{required: true, message: 'Please input testCase Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="docTypeId"
                            label="Document Type Id">
                            <Select
                                placeholder="Select a testCase for recog profile"
                                allowClear>
                                {testCaseState.listDocTypeId}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="captivaId"
                            label="Captiva Id">
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});

export default connect(mapStateToProps, {openPop,closePop,insertTestCase,getTestCase}) (TestCaseCreate);

