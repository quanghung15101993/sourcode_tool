import {Button, Form, Input, Modal,Select} from "antd";
import React from "react";
import {openPop,closePop,insertTemplate,getTemplate} from "../../store/actions/templateAction"
import {connect} from "react-redux";

const {Option} = Select;

function TemplateCreate({templateState,openPop,closePop,insertTemplate,getTemplate}) {
    
    const [form] = Form.useForm();
    const showModal = (value) => {
        form.resetFields();
        openPop(value)
    };

    const handleCancel = () => {
        closePop()
    };


    const onOk = () => {
        form.submit();
    };

    const onFinish = async (dataInsert) => {   
        let body = JSON.stringify({
            "data": {
                "type": "Template",
                "attributes": {
                    "name": dataInsert.name,
                    "doc_type_id": dataInsert.docTypeId,
                    "captiva_id": dataInsert.captivaId,
                }
            }
        })
        console.log(body);
        await insertTemplate(body)
        //Change the records in the Table after user Edit 
        getTemplate()
    };
        return (
            <>
                <Button type="primary" onClick={(value)=>showModal("create")} style={{cssFloat:"right"}}>
                    Create template
                </Button>

                <Modal title="Create template"
                       visible={templateState.isModalVisibleCreate}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={templateState.templateState} onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                    <Form.Item
                            name="name"
                            label="Template Name:"
                            rules={[{required: true, message: 'Please input Template Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="docTypeId"
                            label="Document Type Id">
                            <Select
                                placeholder="Select a Template for recog profile"
                                allowClear>
                                {templateState.listDocTypes.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="captivaId"
                            label="Captiva Id">
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state) => ({
    templateState: state.template,
});

export default connect(mapStateToProps, {openPop,closePop,insertTemplate,getTemplate}) (TemplateCreate);

