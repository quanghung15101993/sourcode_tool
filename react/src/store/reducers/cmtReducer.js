import {CLOSE_POPUP_CMT, POST_CMT, UPLOAD_CMT} from "../types";

const initialState = {
    IsModalVisible: false,
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileLists: [],
    respone: {},
};

const cmtReducer = (state = initialState, action) => {
    //this example handle close popup but present not use
    switch (action.type) {
        case CLOSE_POPUP_CMT:
            return {
                ...state,
                IsModalVisible: action.payload,
            };
        case POST_CMT:
             let file = action.file
             
            return {
                ...state,
                previewImage: file.url || file.preview,
                previewVisible: true,
                previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
                respone: action.payload
            };
        case UPLOAD_CMT:
            if (action.payload.length === 0){
                return {
                    ...state,
                    fileLists: action.payload,
                    previewVisible: false,
                    previewImage: '',
                    respone:{},
                }
            }
            return {
                ...state,
                fileLists: action.payload,
            }
        default:
            return state;
    }
};

export default cmtReducer;
