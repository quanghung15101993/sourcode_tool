import {
  CREATE_IP_PROFILE,
  GET_IP_PROFILES,
  UPDATE_IP_PROFILE,
  RETRIEVE_IP_PROFILE,
} from "../types";

const initialState = {
  dataSource: [],
  dataModify: {},
  dataUpdate: {},
};

function ipProfileReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    // Ip Profile get all profiles.
    case GET_IP_PROFILES:
      return {
        ...state,
        dataSource: payload,
      };

    // Ip Profile create
    case CREATE_IP_PROFILE:
      return {
        ...state,
        dataModify: payload,
      };

    // Ip Profile details
    case RETRIEVE_IP_PROFILE:
      return {
        ...state,
        dataUpdate: payload,
      };

    // Ip Profile update
    case UPDATE_IP_PROFILE:
      return {
        ...state,
        dataModify: payload,
      };

    default:
      return state;
  }
}
export default ipProfileReducer;
