import datetime


class ResponseObject():

    def __init__(self):
        super().__init__()
        self.timestame = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def success(self, data, links=None, relationships=None):
        self.data = data
        self.message = "success"
        self.code = 0
        self.links = links
        self.relationships = relationships
        return self

    def error(self, code=1, links=None, relationships=None, message="error"):
        self.message = message
        self.code = code
        self.links = links
        self.relationships = relationships
        return self
