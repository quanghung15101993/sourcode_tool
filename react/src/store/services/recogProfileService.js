import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const getAll = (params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get("/reg-profiles");
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get("/reg-profiles",request);
};

const get = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/reg-profiles/${id}`);
};

const create = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/reg-profiles", data);
};

const update = (id, data) => {
    return httpAccess(JSON_API_CONTENT_TYPE).put(`/reg-profiles/${id}`, data);
};

const remove = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).delete(`/reg-profiles/${id}`);
};

const getAllProject = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/projects?size=1000");
};

const RecogProfileService = {
    getAll,
    get,
    create,
    update,
    remove,
    getAllProject
};

export default RecogProfileService;