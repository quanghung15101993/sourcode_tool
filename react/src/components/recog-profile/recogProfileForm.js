import {Button, Form, Input, Modal, Select} from "antd";
import React, {useState} from "react";
import {connect} from "react-redux";
import {
    createUpdateRecogProfile,
    insertData,
    updateData,
} from "../../store/actions/recogProfileAction";

const RecogProfileForm = ({recogState, idRecogProfile, createUpdateRecogProfile, insertData, updateData}) => {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const showModalCreate = () => {
        form.resetFields();
        createUpdateRecogProfile({});
        setIsModalVisible(true);
    }
    const showModalEdit = () => {
        form.resetFields();
        createUpdateRecogProfile({id: idRecogProfile.id, form: form});
        setIsModalVisible(true);
    }

    const onOk = () => {
        form.submit();
    };

    const onFinish = (values) => {
        if (recogState.idRecogProfile === "") {
            insertData(values)
        } else {
            updateData({
                id: recogState.idRecogProfile, name: values.name,
                projectId: values.projectId
            });
        }
        setIsModalVisible(false);

    };

    if (idRecogProfile.id === "") {
        return (
            <>
                <Button type="primary" onClick={showModalCreate} key="create" align="end" style={{"float": "right"}}>
                    Create recognition profile
                </Button>
                <Modal title="Create recognition profile"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="Recognition Profile Name:"
                            rules={[{required: true, message: 'Please input Recognition Profile Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="projectId"
                            label="Project:"
                            rules={[{required: true, message: 'Please choose your Project!'}]}>
                            <Select
                                placeholder="Select a Project for recognition profile"
                                allowClear>
                                {recogState.listProject}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    } else {
        return (
            <>
                <Button type="primary" onClick={() => showModalEdit()}>
                    Edit
                </Button>
                <Modal title="Edit recognition profile"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="Recognition Profile Name:"
                            rules={[{required: true, message: 'Please input Recognition Profile Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="projectId"
                            label="Project:"
                            rules={[{required: true, message: 'Please choose your Project!'}]}>
                            <Select
                                placeholder="Select a Project for recognition profile"
                                allowClear>
                                {recogState.listProject}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    }

}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    recogState: state.recogProfileReducer,
    idRecogProfile: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {createUpdateRecogProfile, insertData, updateData})(RecogProfileForm);