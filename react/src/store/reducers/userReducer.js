import {LOGIN, LOGIN_SUCCESS, LOGOUT} from "../types";

const initialState = {
    token: "",
    items: [],
    error: [],
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                token: action.token,
            };
        case LOGIN:
            return {
                ...state,
                token: null
            };
        case LOGOUT:
            return {
                ...state,
                token: null
            };
        default:
            return state
    }
}

export default UserReducer;