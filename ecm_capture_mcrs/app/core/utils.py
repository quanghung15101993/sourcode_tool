from datetime import datetime

'''
    Get date time now
    return: Datetime yyyy-mm-dd HH:mm:ss <string>
'''


def datetime_now():
    return datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
