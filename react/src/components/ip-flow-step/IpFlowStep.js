import React, {useEffect, useState} from "react";
import "antd/dist/antd.css";
import Space from "antd/lib/space";
import {Button, Col, Input, Modal, Row, Table} from 'antd';
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";
import IpFlowStepForm from "./IpFlowStepForm";
import {deleteData, ipFlowStepList} from "../../store/actions/ipFlowStepAction";

const IpFlowStep = ({ipFlowStepState, props, ipFlowStepList, deleteData}) => {
    const [idFlow, setIdFlow] = useState("");
    const {confirm} = Modal;
    const {Search} = Input;

    const columns = [
        {
            title: "Step No.",
            dataIndex: "step",
            key: "step",
        },
        {
            title: "IP Profile",
            dataIndex: "profile_name",
            key: "profile_name",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                    <IpFlowStepForm id={record.id} idFlow={record.ip_flow_id} key={record.id}/>
                    <Button danger onClick={() => removeColumn(record.id)} key={record.id}>
                        Remove
                    </Button>
                </Space>
            ),

        },
    ];
    const onSearch = (value) => {
        console.log(value);
        let id = props.match.params.id;
        ipFlowStepList(id, {
            search: value,
        })
    }

    //useEffect
    useEffect(() => {
        let id = props.match.params.id;
        setIdFlow(id);
        ipFlowStepList(id, {});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        let sort = "";
        if (isNotNullOrUndefined(sorter.field)) {
            sort = sorter.order === "ascend" ? sorter.field : "-" + sorter.field
        }
        let id = props.match.params.id;
        ipFlowStepList(id,
            {
                page: pagination.current,
                sort: sort,
            })
        ;
    };

    const removeColumn = (id) => {
        confirm({
            title: 'Are you sure delete IP Flow Step?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteData({idFlow}, {id: id});
            },
            onCancel() {
            },
        });
    };

    return (
        <>
            <div className="container-fluid">
                <h1 className="h3 mb-4 text-gray-800">Flow ID : {props.match.params.id}</h1>
                <div className="card shadow">
                    <div className="card mb-4">
                        <div className="card-header py-3">
                            <Row>
                                <Col span={12}><Search placeholder="input search text" onSearch={onSearch}
                                                       enterButton/></Col>
                                <Col span={12}>
                                    <IpFlowStepForm id="" idFlow={idFlow} key="create"/>
                                </Col>
                            </Row>
                        </div>
                        <div className="card-body">
                            <Table key="recogProfileTable"
                                   columns={columns}
                                   dataSource={ipFlowStepState.listIpFlowStep}
                                   pagination={{total: ipFlowStepState.total, current: ipFlowStepState.current}}
                                   onChange={handleTableChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    ipFlowStepState: state.IpFlowStepReducer,
    props: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {ipFlowStepList, deleteData})(IpFlowStep);
