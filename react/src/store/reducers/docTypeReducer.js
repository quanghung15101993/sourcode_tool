import {CREATE_UPDATE_DOC_TYPE, DOC_TYPE, VIEW_DOC_TYPE} from "../types";

const initialState = {
    idDocType : "",
    listDocType: [],
    listSingleFields: [],
    listTableFields: [],
    listRecogProfile: [],
    total: 10,
    current: 1,
};

const DocTypeReducer = (state = initialState, action) => {
    switch (action.type) {
        case DOC_TYPE:
            return {
                ...state,
                listDocType : action.payload,
                current: action.page,
                total: action.count
            };
        case VIEW_DOC_TYPE:
            return {
                ...state,
                idDocType : action.id,
                listSingleFields : action.listSingle,
                listTableFields : action.listTable,
            };
        case CREATE_UPDATE_DOC_TYPE:
            return {
                ...state,
                idDocType : action.id,
                listRecogProfile : action.payload,
            };

        default:
            return state;
    }
};

export default DocTypeReducer;
