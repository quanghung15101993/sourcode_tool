import base64
import datetime
import io

import boto3
import six
from botocore.exceptions import ClientError
from decouple import config

from app.core.config.logger import logging

S3_ENDPOINT_URL = config('S3_ENDPOINT')
S3_STORAGE_SYSTEM = config('S3_STORAGE_SYSTEM')
S3_ACCESS_KEY = config('S3_ACCESS_KEY')
S3_SECRET_KEY = config('S3_SECRET_KEY')


class S3Storage:
    def __init__(self):
        try:
            s3_session = boto3.Session(...)
            self.s3_client = s3_session.client(service_name=S3_STORAGE_SYSTEM, use_ssl=False,
                                               endpoint_url=S3_ENDPOINT_URL,
                                               aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
            buckets_result = self.s3_client.list_buckets()
            self.buckets_collection = buckets_result['Buckets']
            print(self.buckets_collection)
            logging.info(" -- Connect to S3 Storage success.")
        except ClientError as e:
            logging.error(" -- Connect to S3 Storage has failed !!!  -- message: {msg}".format(
                msg=e.__str__()))
            raise e

    '''
        Create new bucket
    '''

    def create_new_bucket(self, bucket_name):
        try:
            if not self.is_bucket_exist(bucket_name):
                self.s3_client.create_bucket(Bucket=bucket_name)
            return bucket_name
        except ClientError as e:
            raise e

    '''
        Upload file to s3 storage.
    '''

    def upload_file(self, file_data, file_name, batch_id, bucket_name):
        try:
            # [1] check file upload data & generate file path
            file_path, file_data = self.file_upload_decode(file_upload_data=file_data, file_name=file_name,
                                                           batch_id=batch_id)
            # [2] create bucket if not exist
            s3_bucket = self.create_new_bucket(bucket_name)

            # [3] upload file to this bucket
            self.s3_client.upload_fileobj(file_data, s3_bucket, file_path)
            logging.info("Upload file [{path}] to S3 success!!".format(path=file_path))

            # if upload success, return path file
            full_path_bucket = bucket_name + "/" + file_path

            return full_path_bucket
        except Exception as e:
            logging.error("Upload a file [{file_name}] to S3 error -- message: {err_msg}".format(file_name=file_name,
                                                                                                 err_msg=e.__str__()))
            return None

    '''
        Check bucket exist on s3 server
    '''

    def is_bucket_exist(self, bucket_name):
        # check if no bucket on s3
        if not self.buckets_collection or len(self.buckets_collection) == 0:
            return False
        for bucket in self.buckets_collection:
            if bucket_name == bucket['Name']:
                return True
        return False

    '''
        Build file path by batch id & file extension
    '''

    def build_path_file(self, file_name_ext, batch_id):
        if not file_name_ext or file_name_ext == "":
            return None
        filename = file_name_ext
        # build full path with current datetime & batch id & file name
        now = datetime.datetime.now()
        full_path_file = "{0}/{1}/{2}/{3}/{4}"
        full_path = full_path_file.format(str(now.year), str(now.month), str(now.day), str(batch_id), filename)
        logging.info(full_path)
        return full_path

    '''
        file upload decoded, and get file path
    '''

    def file_upload_decode(self, file_upload_data, file_name, batch_id):
        if isinstance(file_upload_data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in file_upload_data and ';base64,' in file_upload_data:
                # Break out the header from the base64 content
                header, file_upload_data = file_upload_data.split(';base64,')
            # Try to decode the file
            try:
                file_data_decoded = base64.b64decode(file_upload_data)
            except TypeError:
                TypeError('file upload data invalid!!!')

            # Build path file
            full_path_file = self.build_path_file(file_name_ext=file_name, batch_id=batch_id)
            return full_path_file, io.BytesIO(file_data_decoded)
