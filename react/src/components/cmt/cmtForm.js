import {Form, Input} from "antd";
import React from "react";
import {connect} from "react-redux";
import {closePop} from "../../store/actions/cmtAction"


const CMTForm = ({cmtSate}) => {
//this example handle to close popup but present not use
    const handleCancel = () => {
        closePop()
    };
    return (
        <>
            <CustomForm dataImage={cmtSate.respone}/>
        </>
    );
}

function CustomForm(e) {

    let listData = []
    //Fill data into textbox after the image extracted done
    Object.keys(e.dataImage).forEach((key) => {
        listData.push(<div key={key}><Form.Item label={key}>
            <Input maxLength={10} value={e.dataImage[key]}/>{" "}
        </Form.Item></div>)

    })
    return (
            <Form id="bloc2" >
                {listData}
            </Form>
    )
}

//Get state from Store
const mapStateToProps = (state) => ({
    cmtSate: state.cmt,
});
//Connect to store
export default connect(mapStateToProps, {closePop})(CMTForm);
