import {
    CREATE_UPDATE_RECOG_PROFILE,
    RECOG_PROFILE,
} from "../types";

const initialState = {
    idRecogProfile : "",
    listProject: [],
    listRecogProfile: [],
    total: 10,
    current: 1,
};

const recogProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case RECOG_PROFILE:
            return {
                ...state,
                listRecogProfile : action.payload,
                current: action.page,
                total: action.count
            };
        case CREATE_UPDATE_RECOG_PROFILE:
            return {
                ...state,
                idRecogProfile : action.id,
                listProject : action.payload,
            };

        default:
            return state;
    }
};

export default recogProfileReducer;
