import React from "react";
import "antd/dist/antd.css";
import {Upload} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import {postImage, uploadCMT} from "../../store/actions/cmtAction"
import CMTForm from "./cmtForm"
import {connect} from "react-redux";

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const CMT = ({cmtSate, uploadCMT, postImage}) => {

    /*
    When user click to view CMT image, it will handle two step
        - The first will display original image.
        - The second will display data from the image has extracted.
    */
    const handlePreview = (fileList) => async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        let formData = new FormData()
        formData.append("image", file.originFileObj)
        postImage(formData, file)
    };

    const handleChange = (fileList) => {
        console.log(fileList);
        uploadCMT(fileList.fileList)
    };

    const uploadButton = (
        <div>
            <PlusOutlined/>
            <div style={{marginTop: 8}}>Upload</div>
        </div>)
    return (
        <>
            <Upload
                action=""
                listType="picture-card"
                fileList={cmtSate.fileLists}
                onPreview={handlePreview(cmtSate.fileLists)}
                onChange={(e)=>handleChange(e)}
                beforeUpload={() => false}
            >
                {cmtSate.fileLists.length >= 1 ? null : uploadButton}
            </Upload>
            <div className="row">
                <div className="col-md-6">
                    <img alt="" style={{width: '800px'}} src={cmtSate.previewImage}/>
                </div>
                <div className="col-md-6">
                    <CMTForm/>
                </div>
            </div>
            <div>
            </div>
        </>
    );
    // }
}
//Get state from Store
const mapStateToProps = (state) => ({
    cmtSate: state.cmt,
});
//Connect to store
export default connect(mapStateToProps, {postImage, uploadCMT})(CMT);
