import {Button, Form, Input, Modal,Select} from "antd";
import React, {useState} from "react";
import Space from "antd/lib/space";
import {openPop,getTemplate,updateTemplate} from "../../store/actions/templateAction"
import {connect} from "react-redux";

const {Option} = Select;

function TemplateEdit({templateState,templateId,openPop,updateTemplate,getTemplate}) {

    const [form] = Form.useForm();
    //The isModalVisibleEdit can't move on the store because it will happend error when user click open Popup 
    const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
    form.resetFields();
    form.setFieldsValue({
        name: templateState.name, 
        docTypeId: templateState.docTypeId,
        docTypeName: templateState.docTypeName,
        captivaId: templateState.captivaId,
    });
    const onOk = () => {
        form.submit();
    };

    const handleCancel = () => {
        setIsModalVisibleEdit(false)
    };
     const showModal = () => {
         //This openPop function only fill data auto into the Textbox
        openPop(templateId.id)
        //The Popup will arealy open when run to here 
        setIsModalVisibleEdit(true)
    };
   
    const onFinish = async (dataUpdate) =>{   
        let body = JSON.stringify({
            "data": {
                "type": "Template",
                "id": templateId.id,
                "attributes": {
                    "name": dataUpdate.name,
                    "doc_type_id": dataUpdate.docTypeId,
                    "captiva_id": dataUpdate.captivaId,
                }
            }
        })
        await updateTemplate(body,templateId.id)
        setIsModalVisibleEdit(false)
        //Change the records in the Table after user Edit 
            getTemplate()
    };

        return (
            <>
                <Space>
                    <Button type="primary" onClick={showModal}>
                    Edit
                </Button>

                <Modal title="Edit Template"
                       visible={isModalVisibleEdit}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" loading={templateState.loading} onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={(values)=>onFinish(values)}>
                        <Form.Item
                            name="name"
                            label="Template Name:"
                            rules={[{required: true, message: 'Please input Template Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="docTypeId"
                            label="Document Type Id">
                            <Select
                                placeholder="Select a Project for recog profile"
                                allowClear
                            >
                                {templateState.listDocTypes.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="captivaId"
                            label="Captiva Id">
                            <Input maxLength={45}/>
                        </Form.Item>
                    </Form>
                </Modal>
                </Space>
            </>
        );
   
}

//Get state from Store
const mapStateToProps = (state, props) => ({
    templateState: state.template,
    templateId:props
});

export default connect(mapStateToProps, {openPop,getTemplate,updateTemplate}) (TemplateEdit);

