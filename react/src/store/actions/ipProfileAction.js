import IpProfileService from "../services/IpProfileService";
import {
  CREATE_IP_PROFILE,
  GET_IP_PROFILES,
  RETRIEVE_IP_PROFILE,
  UPDATE_IP_PROFILE,
} from "../types";
import isNotNullOrUndefined from "../../common/utils";
import { openNotificationCommon } from "../../common/const";

/**
 * [Action] - get all IP Profile list.
 * @param {*} page
 * @param {*} sort
 * @returns
 */
export const getAllIpProfile = (page, sort) => async (dispatch) => {
  try {
    // get response from api
    const res = await IpProfileService.getAll(page, sort);
    console.log("[ACTION] - Get all ip profile: " + res);

    // Get data list from response
    if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
      responseData = {};
      let dataRes = res.data;
      responseData.total = dataRes.meta.pagination.count;
      responseData.current = dataRes.meta.pagination.page;
      let dataList = [];
      let data = dataRes.data;
      for (let i = 0; i < data.length; i++) {
        dataList.push({
          type: data[i].type,
          id: data[i].id,
          name: data[i].attributes.name,
        });
      }
      responseData.dataList = dataList;
      dispatchHandle(dispatch, GET_IP_PROFILES, responseData);
    } else {
      dispatchHandle(dispatch, GET_IP_PROFILES, "");
    }
  } catch (err) {
    console.log("[ACTION] - getIpProfileList err" + err);
    //ErrorCommon("404","Error");
  }
};

/**
 * [Action] - create new profile
 * @param {*} name
 * @returns
 */
export const createIpProfile = (name) => async (dispatch) => {
  try {
    const response = await IpProfileService.create(name);
    dispatchHandle(dispatch, CREATE_IP_PROFILE, response.data);
    return response.data;
  } catch (err) {
    console.log("[ACTION] - createIpProfile err" + err);
    return Promise.reject(err);
  }
};

/**
 * [Action] Update Image Profile.
 * @param {*} dataUpdate
 * @returns
 */
export const updateIpProfile = (dataUpdate) => async (dispatch) => {
  try {
    const response = await IpProfileService.update(dataUpdate);
    dispatchHandle(dispatch, UPDATE_IP_PROFILE, response.data);
  } catch (err) {
    console.log("[ACTION] - updateIpProfile err" + err);
    openNotificationCommon("error", "Update IP Profile", err.toString());
  }
};

/**
 * [Action] remove ip profile
 * @param {*} id
 * @returns
 */
export const removeIpProfile = (id) => async (dispatch) => {
  try {
    await IpProfileService.remove(id);
  } catch (err) {
    console.log("[ACTION] - removeIpProfile err" + err);
    openNotificationCommon("error", "Remove IP Profile", err.toString());
  }
};

/**
 * [Action] - get ip profile detail
 * @param {*} id
 * @returns
 */
export const getIpProfileDetail = (id) => async (dispatch) => {
  try {
    ipProfileDetail = {};
    const response = await IpProfileService.get(id);
    ipProfileDetail.id = response.data.data.id;
    ipProfileDetail.name = response.data.data.attributes.name;
    await dispatchHandle(dispatch, RETRIEVE_IP_PROFILE, ipProfileDetail);
  } catch (err) {
    console.log("[ACTION] - Get IP Profile detail" + err);
    openNotificationCommon("error", "Get IP Profile", err.toString());
    return Promise.reject(err);
  }
};

/**
 * Get IP Profile from data list
 * @param {*} id
 * @param {*} dataList
 * @returns
 */
export const getIpProfileFromList = (id, dataList) => async (dispatch) => {
  ipProfileDetail = {};

  for (let i = 0; i < dataList.length; i++) {
    if (id === dataList[i].id) {
      ipProfileDetail.id = dataList[i].id;
      ipProfileDetail.name = dataList[i].name;
      break;
    }
  }
  await dispatchHandle(dispatch, RETRIEVE_IP_PROFILE, ipProfileDetail);
  return ipProfileDetail;
};



// Response object api data handle.
let responseData = {
  dataList: [],
  total: 0,
  current: 0,
};

// Response object api data handle.
let ipProfileDetail = {
  id: 0,
  name: "",
};

// dispatch handle.
export const dispatchHandle = (dispatch, type, payloadData) => {
  return dispatch({
    type: type,
    payload: payloadData,
  });
};
