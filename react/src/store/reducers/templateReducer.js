import {GET_TEMPLATE,OPEN_POP_TEMPLATE,CLOSE_POP_PROJECT, INSERT_TEMPLATE, CLOSE_POP_TEMPLATE} from "../types";

const initialState = {
    dataSource: [],
    loading: false,
    total: 10,
    current: 1,
    isModalVisibleCreate: false,
    listDocTypes: [],
    name:"",
    docTypeId:"",
    docTypeName:"",
    captivaId:""
};

const templateReducer = (state = initialState, action) => {
    //this example handle close popup but present not use
    switch (action.type) {
        case GET_TEMPLATE:
            //Check page undefined
            if (action.page) {
                
                return {
                    ...state,
                    current: action.page,
                    dataSource: action.payload,
                    loading: false,
                    total: action.count
                };
            }

            return {
                ...state,  
                listDocTypes:action.dataDoctype,
                dataSource: action.payload,
                loading: false,
                total: action.count
            };

            case OPEN_POP_TEMPLATE:
                if (action.value ==="create") {
                    return{
                        ...state,
                        isModalVisibleCreate: true,
                    };
                }
                let data = action.payload.data.data.attributes
                return {
                    ...state,
                    name: data.name,
                    docTypeId: data.doc_type_id,
                    docTypeName: data.doc_type_name,
                    captivaId: data.captiva_id,
                };

            case INSERT_TEMPLATE:
                return {
                    ...state,      
                    isModalVisibleCreate: false,
                };
                    
            case CLOSE_POP_TEMPLATE:
                return {
                    ...state,
                    isModalVisibleCreate:false,
                };      
        default:
            return state;
    }
};

export default templateReducer;
