import React from "react";

function BreadcrumbCommon(props) {
    return (
        <>
            <h1 className="h3 mb-4 text-gray-800">{props.page}</h1>
        </>);
}

export default BreadcrumbCommon;
