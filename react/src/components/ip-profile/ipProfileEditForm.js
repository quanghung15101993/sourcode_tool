import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Form, Input, Modal } from "antd";
import isNotNullOrUndefined from "../../common/utils";
import { openNotificationCommon } from "../../common/const";

import {
  getAllIpProfile,
  updateIpProfile,
} from "../../store/actions/ipProfileAction";

const IpProfileEditForm = (props) => {
  // init Edit modal visible
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [ipProfileDetail, setIpProfileDetail] = useState({});

  const [form] = Form.useForm();
  const dispatch = useDispatch();

  /**
   * [Form config] - show model form
   */
  const showModal = () => {
    // reset form field
    form.resetFields();

    // binding data value for edit form.
    if (isNotNullOrUndefined(props.value)) {
      setIpProfileDetail(props.value);
      form.setFieldsValue({
        name: props.value.name,
      });
    }
    // open form modal
    openFormModal();
  };

  /**
   * [Modal config] - close form modal.
   */
  const closeFormModal = () => {
    setEditModalVisible(false);
  };

  /**
   * [Modal config] - Open form modal.
   */
  const openFormModal = () => {
    setEditModalVisible(true);
  };

  /**
   * [Form] - form submit action
   */
  const onSubmitted = () => {
    form.submit();
  };

  /**
   * [Form] - form submitted & update ip profile
   *
   * @param {*} values
   */
  const onFinish = (values) => {
    console.log("onFinish form: " + values);
    if (!isNotNullOrUndefined(values)) {
      openNotificationCommon(
        "error",
        "Edit IP Profile",
        "IP Profile update data invalid!"
      );
    }

    // prepare model to update
    let dataUpdate = {};
    if (isNotNullOrUndefined(ipProfileDetail)) {
      dataUpdate = ipProfileDetail;
      dataUpdate.name = values.name;
      // update data
      updateIpProfileAction(dataUpdate);
    }
  };

  /**
   * Update IP Profile
   * @param {*} dataUpdate
   */
  const updateIpProfileAction = (dataUpdate) => {
    console.log("Create IP Profile name: " + dataUpdate);
    dispatch(updateIpProfile(dataUpdate)).then(() => {
      openNotificationCommon(
        "success",
        "Edit Image Profile",
        "Update image process profile success!"
      );
      // reload list ip profile changed.
      dispatch(getAllIpProfile()).then(() => {
        closeFormModal();
      });
    });
  };

  /**
   * Render EDIT forms
   */
  return (
    <>
      <Button
        type="primary"
        onClick={showModal}
        style={{ cssFloat: "right" }}
        className="btn btn-outline-primary"
      >
        Edit
      </Button>

      <Modal
        forceRender
        title="Edit IP Profile"
        visible={editModalVisible}
        onOk={onSubmitted}
        onCancel={closeFormModal}
        footer={[
          <Button key="submit" type="primary" onClick={onSubmitted}>
            Update
          </Button>,
          <Button key="back" danger onClick={closeFormModal}>
            Cancel
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="ipProfileForm"
          onFinish={onFinish}
        >
          <Form.Item
            name="name"
            label="Profile Name:"
            rules={[{ required: true, message: "Please input profile name!" }]}
          >
            <Input maxLength={45} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default IpProfileEditForm;
