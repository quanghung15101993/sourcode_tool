import React, {useEffect} from "react";
import "antd/dist/antd.css";
import {connect} from "react-redux";
import TestCaseForm from '../test-cases/testCaseForm'
import TestCaseAttachFile from "./testCaseAttachFile";
import TestCaseAssertData from "./testCaseAssertData";


function TestCase({testCaseState}) {

    return (
        <>
            <div className="card mb-4 card-body">
                <TestCaseForm/>
                <div style={{display: `${testCaseState.displayUpload}`}}>
                    <hr></hr>
                    <TestCaseAttachFile/>
                </div>
                <div style={{display: `${testCaseState.displayAssertData}`}}>
                    <hr></hr>
                    <TestCaseAssertData/>
                </div>
            </div>
        </>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});
//Connect to store
export default connect(mapStateToProps, {})(TestCase);
