import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const getAll = (params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get("/extraction");
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get("/extraction",request);
};

const get = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/extraction/${id}`);
};

const create = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/extraction", data);
};

const update = (id, data) => {
    return httpAccess(JSON_API_CONTENT_TYPE).put(`/extraction/${id}`, data);
};

const remove = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).delete(`/extraction/${id}`);
};

const getAllProject = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/projects?size=1000");
};

const ExtractionService = {
    getAll,
    get,
    create,
    update,
    remove,
    getAllProject
};

export default ExtractionService;