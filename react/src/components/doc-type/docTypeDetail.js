import {Button, Modal, Table} from "antd";
import React, {useState} from "react";
import {connect} from "react-redux";
import {detailDocType} from "../../store/actions/docTypeAction";

const DocTypeDetail = ({docTypeState, idDocType, detailDocType}) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const showModalDetail = () => {
        detailDocType({id: idDocType.id});
        setIsModalVisible(true);
    }

    const columnsSingle = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Type",
            dataIndex: "type",
            key: "type",
            sorter: true,
        },
    ];
    const columnsTable = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Section Name",
            dataIndex: "section_name",
            key: "section_name",
        },
        {
            title: "Type",
            dataIndex: "type",
            key: "type",
        },
    ];


    return (
        <>
            <Button type="primary" onClick={showModalDetail} key="create" align="end" style={{"float": "right"}}>
                View
            </Button>
            <Modal title="Detail Doc Type"
                   visible={isModalVisible}
                   onOk={handleCancel}
                   onCancel={handleCancel}
                   width={1000}
                   footer={[
                       <Button key="back" onClick={handleCancel}>
                           Close
                       </Button>,
                   ]}>
                <h6>Single field</h6>
                <Table rowKey="name"
                       columns={columnsSingle}
                       dataSource={docTypeState.listSingleFields}
                       pagination={false}
                       bordered
                       scroll={{ y: 240 }}
                />
                <hr></hr>
                <h6>Table field</h6>
                <Table rowKey="name"
                       columns={columnsTable}
                       dataSource={docTypeState.listTableFields}
                       pagination={false}
                       bordered
                       scroll={{ y: 240 }}
                />
            </Modal>
        </>
    );
}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    docTypeState: state.docTypeReducer,
    idDocType: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {detailDocType})(DocTypeDetail);