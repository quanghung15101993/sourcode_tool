import {Button, Form, Input, Modal, Select} from "antd";
import React, {useState} from "react";
import {connect} from "react-redux";
import {createUpdateDocType, insertData, updateData} from "../../store/actions/docTypeAction";

const DocTypeForm = ({docTypeState, idDocType, createUpdateDocType, insertData, updateData}) => {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const showModalCreate = () => {
        form.resetFields();
        createUpdateDocType({});
        setIsModalVisible(true);
    }
    const showModalEdit = () => {
        form.resetFields();
        createUpdateDocType({id: idDocType.id, form: form});
        setIsModalVisible(true);
    }

    const onOk = () => {
        form.submit();
    };

    const onFinish = (values) => {
        if (idDocType.id === "") {
            insertData(values)
        } else {
            updateData({
                id: idDocType.id, name: values.name,
                recogProfile: values.recogProfile
            });
        }
        setIsModalVisible(false);

    };

    if (idDocType.id === "") {
        return (
            <>
                <Button type="primary" onClick={showModalCreate} key="create" align="end" style={{"float": "right"}}>
                    Create Doc Type
                </Button>
                <Modal title="Create Doc Type"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="Doc Type Name:"
                            rules={[{required: true, message: 'Please input Doc Type Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="recogProfile"
                            label="Recognition Profile:"
                            rules={[{required: true, message: 'Please choose your Recognition Profile!'}]}>
                            <Select
                                placeholder="Select a Recognition Profile for Doc Type"
                                allowClear>
                                {docTypeState.listRecogProfile}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    } else {
        return (
            <>
                <Button type="primary" onClick={() => showModalEdit()}>
                    Edit
                </Button>
                <Modal title="Edit Doc Type"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="Doc Type Name:"
                            rules={[{required: true, message: 'Please input Doc Type Name!'}]}>
                            <Input maxLength={45} disabled/>
                        </Form.Item>

                        <Form.Item
                            name="recogProfile"
                            label="Recognition Profile:"
                            rules={[{required: true, message: 'Please choose your Recognition Profile!'}]}>
                            <Select
                                placeholder="Select a Recognition Profile for Doc Type"
                                allowClear>
                                {docTypeState.listRecogProfile}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    }

}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    docTypeState: state.docTypeReducer,
    idDocType: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {createUpdateDocType, insertData, updateData})(DocTypeForm);