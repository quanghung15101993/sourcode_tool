from typing import Optional

from bson import ObjectId

from .repositories_helper import batch_model_helper
from ..core.base import base_repository

'''
    Get collection of batch 
'''
batch_collection = base_repository.get_collection('batch')

'''
    Get all batchs
'''


async def retrieve_batch():
    batch_data = []
    async for batch in batch_collection.find():
        batch_data.append(batch_model_helper(batch))
    return batch_data


async def create_batch(batch_data: dict) -> dict:
    batch = await batch_collection.insert_one(batch_data)
    new_batch = await batch_collection.find_one({"_id": batch.inserted_id})
    return batch_model_helper(new_batch)


'''
    retrieves batch by id
'''


async def retrieve_batch_repo(b_id: str, return_dict: Optional[int] = None):
    batch = await batch_collection.find_one({"_id": ObjectId(b_id)})
    if return_dict and return_dict == 1:
        return batch_model_helper(batch)
    return batch
