import {Button, Form, Input, Modal, Select} from "antd";
import React, {useState} from "react";
import {connect} from "react-redux";
import {createUpdateIpFlow, insertData, updateData} from "../../store/actions/ipFlowAction";

const IpFlowForm = ({ipFlowState, idFlow, createUpdateIpFlow, insertData, updateData}) => {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const showModalCreate = () => {
        form.resetFields();
        createUpdateIpFlow({});
        setIsModalVisible(true);
    }
    const showModalEdit = () => {
        form.resetFields();
        createUpdateIpFlow({id: idFlow.id, form: form});
        setIsModalVisible(true);
    }

    const onOk = () => {
        form.submit();
    };

    const onFinish = (values) => {
        if (idFlow.id === "") {
            insertData(values)
        } else {
            updateData({
                id: idFlow.id, name: values.name,
                projectId: values.projectId
            });
        }
        setIsModalVisible(false);

    };

    if (idFlow.id === "") {
        return (
            <>
                <Button type="primary" onClick={showModalCreate} key="create" align="end" style={{"float": "right"}}>
                    Create IP Flow
                </Button>
                <Modal title="Create IP Flow"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="IP Flow Name:"
                            rules={[{required: true, message: 'Please input IP Flow Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="projectId"
                            label="Project:"
                            rules={[{required: true, message: 'Please choose your Project!'}]}>
                            <Select
                                placeholder="Select a Project for IP Flow"
                                allowClear>
                                {ipFlowState.listProject}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    } else {
        return (
            <>
                <Button type="primary" onClick={() => showModalEdit()}>
                    Edit
                </Button>
                <Modal title="Edit IP Flow"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="name"
                            label="IP Flow Name:"
                            rules={[{required: true, message: 'Please input IP Flow Name!'}]}>
                            <Input maxLength={45}/>
                        </Form.Item>

                        <Form.Item
                            name="projectId"
                            label="Project:"
                            rules={[{required: true, message: 'Please choose your Project!'}]}>
                            <Select
                                placeholder="Select a Project for IP Flow"
                                allowClear>
                                {ipFlowState.listProject}
                            </Select>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    }

}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    ipFlowState: state.ipFlowReducer,
    idFlow: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {createUpdateIpFlow, insertData, updateData})(IpFlowForm);