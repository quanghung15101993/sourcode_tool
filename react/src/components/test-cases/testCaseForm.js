import { Form, Input,  Select, Row, Col} from "antd";
import React, {useEffect} from "react";
import {getTestCase, updateTestCase, getProject, projectOptinon, flowOption,getDescripValue} from "../../store/actions/testCaseAction"
import {connect} from "react-redux";
import $ from "jquery";

const {Option} = Select;
const {TextArea} = Input;

function TestCaseForm({testCaseState, projectOptinon, getProject, flowOption, getDescripValue}) {

    const [form] = Form.useForm();

    //useEffect
    useEffect(() => {
        form.resetFields();
        getProject()
    }, []);


    function onChangeProject(projectId) {
        $("#original_img_prev_tif").remove()
        $("#result_img_prev_tif").remove()
        projectOptinon(projectId);
    }

    function onChangeFlow(flowId) {
        $("#original_img_prev_tif").remove()
        $("#result_img_prev_tif").remove()
        flowOption(flowId);
    }

    function onChangeDescrip(e) { 
         let value = e.target.value;
         getDescripValue(value);
    }

    return (
        <>
            <Form form={form} name="userForm">
                <Row>
                    <Col span={12}>
                        <Form.Item
                            label="Project Name">
                            <Select
                                placeholder="Select a Project"
                                onChange={(e) => onChangeProject(e)}
                                allowClear
                                value={testCaseState.projectFlowId}>
                                {testCaseState.projectFlowData.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={12} style={{paddingLeft:'15px'}} >
                        <Form.Item
                            label="Flow">
                            <Select
                                placeholder="Select a Flow"
                                allowClear
                                onChange={(e) => onChangeFlow(e)}
                                value={testCaseState.flowProjectId}
                            >
                                {testCaseState.flowProjectData.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.ip_flow_name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            name="description"
                            label="Description:"
                            onChange ={(e)=>onChangeDescrip(e)}>
                            <TextArea/>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>

        </>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});

export default connect(mapStateToProps, {
    getTestCase,
    updateTestCase,
    getProject,
    projectOptinon,
    flowOption,
    getDescripValue,
})(TestCaseForm);

