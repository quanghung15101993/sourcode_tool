import {Form, Select, Row, Col,Button} from "antd";
import React, {useEffect} from "react";
import {getProject, recogDoctypeOption,doctypeTemplateOption,templateOption,runTestCase} from "../../store/actions/testCaseAction"
import {connect} from "react-redux";
import TestCaseInputData from "./testCaseInputData"
import isNotNullOrUndefined from "../../common/utils";
import $ from "jquery";
import Tiff from "tiff.js";

const {Option} = Select;

function TestCaseAssertData({testCaseState,getProject, recogDoctypeOption,doctypeTemplateOption,templateOption,runTestCase}) {

    const [form] = Form.useForm();    
 
    useEffect(() => {
        form.resetFields();
        getProject()
    }, []);


    function onChangeRecogProfile(recogId) {
        if (isNotNullOrUndefined(testCaseState.imageTiifCanvas)) {
            $('#canvas-input-data-tif').remove()
        }
        recogDoctypeOption(recogId);
    }
    // Doctype param is a Array which have doctypeNam and doctypeId fields
    function onChangeDoctype(doctype) {
    
        if (isNotNullOrUndefined(testCaseState.imageTiifCanvas)) {
            $('#canvas-input-data-tif').remove()
        }
        doctypeTemplateOption(doctype[0], doctype[1])  
    }
    function onChangeTemplate(templateId) {
        let imageTiifCanvas = ""
        if (isNotNullOrUndefined(testCaseState.imageInputData) && 
        isNotNullOrUndefined(testCaseState.imageInputData[0])) {
            var reader = new FileReader();
                reader.onload = function (e) {
                    // get loaded data and render thumbnail, file size max 100Mb.
                    Tiff.initialize({
                        TOTAL_MEMORY: 100000000
                    });
                    var tiff = new Tiff({
                        buffer: e.target.result
                    });        
                    var tiffCanvas = tiff.toCanvas();
                    $(tiffCanvas).css({
                        "margin-top": "10%",
                        "max-width": "100%",
                        "width": "100%",
                        "height": "100%",
                        "display": "block",
                        "padding-top": "2%"
                    }).addClass("preview-tif").attr('id', 'canvas-input-data-tif');
                    //  $('#processed-image-tif').append(tiffCanvas); //btn-upload-image
                     document.getElementById('#image-input-data-tif').append(tiffCanvas)
                     imageTiifCanvas = tiffCanvas
                     templateOption(templateId,testCaseState.doctypeId,imageTiifCanvas)
                };
                reader.readAsArrayBuffer(testCaseState.imageInputData[0].originFileObj); 
        }
        
        
    }

    function handleRunTestCase(){
        if (isNotNullOrUndefined(testCaseState.imageTiifCanvas)) {
            $('#canvas-input-data-tif').remove()
        }
        runTestCase(testCaseState.testCaseId)    
    }
    
    return (
        <>
            <Form form={form} name="userForm">
                
                <Row>
                    {/*RecogProfile*/}
                    <Col span={12} style={{paddingLeft:'15px'}} >
                        <Form.Item
                            label="Recog Profile">
                            <Select
                                placeholder="Select a Recog Profile"
                                allowClear
                                onChange={(e) => onChangeRecogProfile(e)}
                                value={testCaseState.recogId}
                            >
                                {testCaseState.recogData.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>  
                    {/*Doctype*/}
                    <Col span={12} style={{paddingLeft:'15px'}} >
                        <Form.Item
                            label="Doctype">
                            <Select
                                placeholder="Select a Doctype"
                                allowClear
                                onChange={(e) => onChangeDoctype(e)}
                                value={testCaseState.doctypeName}
                            >
                                {testCaseState.doctypeData.map((data, i) => (
                                    <Option key={i} value={[data.id,data.attributes.name]} >{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                     {/*Template*/}
                     <Col span={12} style={{paddingLeft:'15px'}} >
                        <Form.Item
                            label="Template">
                            <Select
                                placeholder="Select a Template"
                                allowClear
                                onChange={(e) => onChangeTemplate(e)}
                                value={testCaseState.templateId}
                            >
                                {testCaseState.templateData.map((data, i) => (
                                    <Option key={i} value={data.id}>{data.attributes.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>                              
                </Row>   
                
            </Form>
            <br />
            <div className="row">
                <div className="col-md-6">
                    <TestCaseInputData />
                </div>
                <div className="col-md-6">
                    <Form form={form} layout="vertical" name="testCaseDataResult">
                        <div style={{display: `${testCaseState.displayRunTestCaseBt}`}} className= "bt-center">    
                            <Form.Item>
                                <Button type="primary" onClick={()=>handleRunTestCase()}>Run TestCase</Button>
                            </Form.Item>
                        </div>
                        <div id ="#image-input-data-tif" ></div>            
                        {testCaseState.listDataResult}
                    </Form>
                </div>
            </div>
        </>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});

export default connect(mapStateToProps, {
    getProject,
    recogDoctypeOption, 
    doctypeTemplateOption,
    templateOption,
    runTestCase
})(TestCaseAssertData);

