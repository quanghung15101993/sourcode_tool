import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  createIpProfile,
  getAllIpProfile,
} from "../../store/actions/ipProfileAction";
import { Button, Form, Input, Modal } from "antd";
import { openNotificationCommon } from "../../common/const";

const IpProfileAddForm = () => {
  
  // init Ip Profile hook
  const [createModalVisible, setCreateModalVisible] = useState(false);
  const dispatch = useDispatch();

  // init form
  const [form] = Form.useForm();

  /**
   * [Form config] - show model form
   */
  const showModal = () => {
    // reset form field
    form.resetFields();
    openFormModal();
  };

  /**
   * [Modal config] - close form modal.
   */
  const closeFormModal = () => {
    setCreateModalVisible(false);
  };

  /**
   * [Modal config] - Open form modal.
   */
  const openFormModal = () => {
    setCreateModalVisible(true);
  };

  /**
   * [Form config] - On submitted form
   */
  const onSubmitted = () => {
    form.submit();
  };

  const onFinish = (values) => {
    console.log("onFinish form: " + values);
    saveIpProfile(values);
  };

  /**
   * Create Image Process Profile.
   *
   * @param {*} values
   */
  const saveIpProfile = (values) => {
    console.log("Create IP Profile name: " + values);
    dispatch(createIpProfile(values))
      .then(() => {
        openNotificationCommon(
          "success",
          "New Image Profile",
          "Create new image process profile success!"
        );
        // reload table after add
        dispatch(getAllIpProfile()).then(() => {
          closeFormModal();
        });
      })
      .catch((e) => {
        console.log(e);
        openNotificationCommon("error", "Create IP Profile", e.toString());
      });
  };

  /**
   * Render CREATE forms
   */
  return (
    <>
      <Button
        type="primary"
        onClick={showModal}
        style={{ cssFloat: "right" }}
        className="btn btn-outline-primary"
      >
        New Profile
      </Button>

      <Modal
        forceRender
        title="Create New IP Profile"
        visible={createModalVisible}
        onOk={onSubmitted}
        onCancel={closeFormModal}
        footer={[
          <Button key="submit" type="primary" onClick={onSubmitted}>
            Create
          </Button>,
          <Button key="back" danger onClick={closeFormModal}>
            Cancel
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="ipProfileForm"
          onFinish={onFinish}
        >
          <Form.Item
            name="name"
            label="Profile Name:"
            rules={[{ required: true, message: "Please input profile name!" }]}
          >
            <Input maxLength={45} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default IpProfileAddForm;
