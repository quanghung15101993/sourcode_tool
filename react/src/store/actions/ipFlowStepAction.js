import {IP_FLOW_STEP, CREATE_UPDATE_IP_FLOW_STEP} from "../types";
import React from "react";
import {Select} from "antd";

import {openNotificationCommon, startLoading, stopLoading} from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";
import IpFlowStepService from "../services/ipFlowStepService";

const {Option} = Select;

export const ipFlowStepList = (idFlow, params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await IpFlowStepService.getAll(idFlow, params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let count = repos.data.meta.pagination.count;
            let dataList = repos.data.data;
            let list = [];
            for (let i = 0; i < dataList.length; i++) {
                list.push({
                    key : dataList[i].id,
                    id: dataList[i].id,
                    type: dataList[i].type,
                    project_id: dataList[i].attributes.project_id,
                    project_name: dataList[i].attributes.project_name,
                    flow_step_id: dataList[i].attributes.flow_step_id,
                    ip_flow_id: dataList[i].attributes.ip_flow_id,
                    ip_flow_name: dataList[i].attributes.ip_flow_name,
                    profile_id: dataList[i].attributes.profile_id,
                    profile_name: dataList[i].attributes.profile_name,
                    step: dataList[i].attributes.step,
                });
            }
            dispatch({
                type: IP_FLOW_STEP,
                count: count,
                page: params.page,
                payload: list,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

export const createUpdateIpFlowStep = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await IpFlowStepService.getAllIPProject();
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let dataIPProject = repos.data.data;
            let list = [];
            for (let i = 0; i < dataIPProject.length; i++) {
                list.push(
                    <Option value={dataIPProject[i].id} key={dataIPProject[i].id}>{dataIPProject[i].attributes.name}</Option>
                );
            }
            if (params.id == undefined || params.id == "") {
                dispatch({
                    type: CREATE_UPDATE_IP_FLOW_STEP,
                    payload: list,
                    id: "",
                })
            } else {
                const dataRecog = await IpFlowStepService.get(params.idFlow, params.id);
                if (isNotNullOrUndefined(dataRecog) && isNotNullOrUndefined(dataRecog.data)) {
                    let data = dataRecog.data.data.attributes;
                    let profileId = data.profile_id + "";
                    params.form.setFieldsValue({
                        profile: profileId,
                        step: data.step,
                    });
                    dispatch({
                        type: CREATE_UPDATE_IP_FLOW_STEP,
                        payload: list,
                        id: params.id,
                    })
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// Insert
export const insertData = (idFlow, params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "IpFlowStep",
                "attributes":
                    {
                        "ip_flow_id": idFlow,
                        "ip_profile_id": params.profile,
                        "order": params.step
                    }
            }
        });
        const res = await IpFlowStepService.create(data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
            // reset table
            const repos = await IpFlowStepService.getAll(idFlow, {});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

// Update
export const updateData = (idFlow, params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "IpFlowStep",
                "id": params.id,
                "attributes": {
                    "ip_flow_id": idFlow,
                    "ip_profile_id": params.ip_profile_id,
                    "order": params.step
                }
            }
        });
        const res = await IpFlowStepService.update(params.id, data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Update bản ghi', 'Cập nhật thành công bản ghi');
            // reset tables
            const repos = await IpFlowStepService.getAll(idFlow, {});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Update bản ghi', 'Cập nhật không thành công bản ghi');
    }
    stopLoading();
};

// Delete
export const deleteData = (idFlow, params) => async (dispatch) => {
    startLoading();
    try {
        const res = await IpFlowStepService.remove(params.id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
            // reset tables
            const repos = await IpFlowStepService.getAll(Object.values(idFlow)[0], {});
            resetTable(repos, dispatch);
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
    stopLoading();
};

// reset table
const resetTable = (repos, dispatch) => {
    if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
        console.log(repos);
        let count = repos.data.meta.pagination.count;
        let dataList = repos.data.data;
        let list = [];
        for (let i = 0; i < dataList.length; i++) {
            list.push({
                key : dataList[i].id,
                id: dataList[i].id,
                type: dataList[i].type,
                project_id: dataList[i].attributes.project_id,
                project_name: dataList[i].attributes.project_name,
                flow_step_id: dataList[i].attributes.flow_step_id,
                ip_flow_id: dataList[i].attributes.ip_flow_id,
                ip_flow_name: dataList[i].attributes.ip_flow_name,
                profile_id: dataList[i].attributes.profile_id,
                profile_name: dataList[i].attributes.profile_name,
                step: dataList[i].attributes.step,
            });
        }
        dispatch({
            type: IP_FLOW_STEP,
            count: count,
            page: 1,
            payload: list,
        })

    }
};




