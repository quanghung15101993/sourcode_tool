from app.core.config.logger import logging

from app.models.batch import BatchModel
from app.repositories.batch_repositoy import retrieve_batch_repo
from app.repositories.file_upload_repository import create_file_upload_repo, get_upload_result_by_batch_repo
from app.repositories.repositories_helper import file_upload_result_helper
from ..dependencies.s3_storage_depends import S3_ENDPOINT_URL

'''
        Build upload result dict response
    '''


def build_upload_result_response(result) -> dict:
    s3_endpoint = S3_ENDPOINT_URL + "/"
    return {
        "file_name": result['file_name'],
        "file_url": s3_endpoint + result['file_path'],
        "status": result['status'],
        "upload_date": result['upload_date']
    }


'''
    Get batch by id
'''


async def get_batch_by_id_dp(batch_id: str) -> BatchModel:
    try:
        batch_detais = await retrieve_batch_repo(batch_id, return_dict=0)
        return batch_detais
    except:
        raise Exception


'''
    Create file upload result
'''


async def create_file_upload_result(file_upload, s3_file_path, batch_id, status=False):
    try:
        file_upload_result = file_upload_result_helper(file_upload=file_upload, s3_file_path=s3_file_path,
                                                       batch_id=batch_id, status=True)
        await create_file_upload_repo(file_upload_data=file_upload_result)
    except Exception as e:
        logging.info("Create file upload error! -- message {msg}.".format(msg=e.__str__()))


'''
    Get upload result by id
'''


async def get_upload_result_srv(batch_id):
    try:
        result_success_arr = []
        result_error_arr = []

        # filter upload result by this batch
        upload_results = await get_upload_result_by_batch_repo(batch_id=batch_id)
        if not upload_results or len(upload_results) == 0:
            raise Exception("Batch [{b_id}] not file upload".format(b_id=batch_id))
        # filter upload result with success and error case
        for result in upload_results:
            if result['status']:
                result_success_arr.append(build_upload_result_response(result))
            else:
                result_error_arr.append(result)
        result_response = {
            "upload_success": result_success_arr,
            "upload_error": result_error_arr
        }
        return result_response
    except Exception as e:
        logging.info("Create file upload error! -- message {msg}.".format(msg=e.__str__()))
        raise e
