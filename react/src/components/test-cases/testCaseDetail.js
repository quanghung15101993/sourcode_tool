import React, {useState} from "react";
import "antd/dist/antd.css";
import {Button, Col, Modal, Row, Table, Typography} from 'antd';
import {connect} from "react-redux";
import {getTestCaseDetail} from "../../store/actions/testCaseAction";

const TestCaseDetail = ({testCaseState, idTestCase, getTestCaseDetail}) => {
    const {Text} = Typography;
    const [isModalVisible, setIsModalVisible] = useState(false);
    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const showModal = () => {
        getTestCaseDetail(idTestCase.id)
        setIsModalVisible(true);
    }

    const columns = [
        {
            title: "No.",
            dataIndex: "id",
            key: "id",
            width: "60px"
        },
        {
            title: 'Extract Expect',
            children: [
                {
                    title: 'Name',
                    dataIndex: 'extract_expect_name',
                    key: 'extract_expect_name',
                },
                {
                    title: 'Value',
                    dataIndex: 'extract_expect_value',
                    key: 'extract_expect_value',
                },
            ],
        },
        {
            title: 'Extract Result',
            children: [
                {
                    title: 'Name',
                    dataIndex: 'extract_result_name',
                    key: 'extract_result_name',
                },
                {
                    title: 'Value',
                    dataIndex: 'extract_result_value',
                    key: 'extract_result_value',
                },
            ],
        },
    ];

    return (
        <>
            <Button type="primary" onClick={showModal} key="create" align="end" style={{"float": "right"}}>
                View
            </Button>
            <Modal className="fadeModel"
                   title="Detail Test Case"
                   visible={isModalVisible}
                   onCancel={handleCancel}
                   width={1000}
                   footer={[
                       <Button key="back" onClick={handleCancel}>
                           Close
                       </Button>,
                   ]}>
                <div className="card mb-4">
                    <div className="card-header py-3">
                        <Row>
                            <Col span={12}>Project Name : {testCaseState.testCaseDetail.project_name} </Col>
                            <Col span={12}>Flow : {testCaseState.testCaseDetail.flow_name}</Col>
                            <Col span={24}>Description : {testCaseState.testCaseDetail.testcase_description}</Col>
                        </Row>
                    </div>
                </div>
                <div className="card mb-4">
                    <div className="card-header py-3">
                        <Row>
                            <Col span={24} id={idTestCase.id}>
                                <div id="#original_image_content" align="center"></div>
                            </Col>
                            {/*<Col span={12}>*/}
                            {/*    <div id="#image_result_content" align="center"></div>*/}
                            {/*</Col>*/}
                        </Row>
                    </div>
                </div>
                <div className="card mb-4">
                    <div className="card-header py-3">
                        <Row>
                            <Col span={12}>Recog Profile : {testCaseState.testCaseDetail.recog_profile_name}</Col>
                            <Col span={12}>Doctype : {testCaseState.testCaseDetail.doctype_name}</Col>
                            <Col span={24}>Template : {testCaseState.testCaseDetail.template_name}</Col>
                        </Row>
                    </div>
                </div>

                <div className="card mb-4">
                    <Table key="id"
                           columns={columns}
                           dataSource={testCaseState.tableTestCaseDetail}
                           pagination={false}
                           bordered
                           scroll={{y: 300}}
                    />
                </div>
            </Modal>
        </>

    );
}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    testCaseState: state.testCase,
    idTestCase: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {getTestCaseDetail})(TestCaseDetail);
