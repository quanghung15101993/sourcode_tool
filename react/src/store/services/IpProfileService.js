import { httpAccess, JSON_API_CONTENT_TYPE } from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const IP_PROFILE_TYPE = "IpProfile";

/**
 * Get all ip profile services, with params [page, sort, search] optional
 * @param {*} params  contains {page, sort, search}
 * @returns  list ip profile
 */
const getAll = (params) => {
  let parameters = new URLSearchParams();
  if (!isNotNullOrUndefined(params)) {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/ip-profiles");
  }

  if (isNotNullOrUndefined(params.page)) {
    parameters.append("page", params.page);
  }
  if (isNotNullOrUndefined(params.sort)) {
    parameters.append("sort", params.sort);
  }
  if (isNotNullOrUndefined(params.search)) {
    parameters.append("filter[search]", params.search);
  }
  let request = {
    params: parameters,
  };
  return httpAccess(JSON_API_CONTENT_TYPE).get("/ip-profiles", request);
};

const get = (id) => {
  return httpAccess(JSON_API_CONTENT_TYPE).get(`/ip-profiles/${id}`);
};

/**
 * Create new ip profile service.
 * @param {*} data request data
 * @returns
 */
const create = (data) => {
  if (isNotNullOrUndefined(data)) {
    let requestBody = JSON.stringify({
      data: {
        type: IP_PROFILE_TYPE,
        attributes: { name: data.name },
      },
    });
    return httpAccess(JSON_API_CONTENT_TYPE).post("/ip-profiles", requestBody);
  }
  return httpAccess(JSON_API_CONTENT_TYPE).post("/ip-profiles", "");
};

/**
 * Update profile service.
 * @param {*} data
 * @returns
 */
const update = (data) => {
  if (isNotNullOrUndefined(data)) {
    let requestBody = JSON.stringify({
      data: {
        type: IP_PROFILE_TYPE,
        id: data.id,
        attributes: { name: data.name },
      },
    });
    return httpAccess(JSON_API_CONTENT_TYPE).put(
      `/ip-profiles/${data.id}`,
      requestBody
    );
  }
  //return httpAccess(JSON_API_CONTENT_TYPE).put(`/ip-profiles/${data.id}`, "");
};

const remove = (id) => {
  return httpAccess(JSON_API_CONTENT_TYPE).delete(`/ip-profiles/${id}`);
};

const IpProfileService = {
  getAll,
  get,
  create,
  update,
  remove,
};

export default IpProfileService;
