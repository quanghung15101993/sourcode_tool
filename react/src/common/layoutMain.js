import React, {useEffect} from "react";
import {Checkbox, Form, Input, Layout} from "antd";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import ContentCommon from "./content";
import FooterCommon from "./footer";
import HeaderCommon from "./header";
import BreadcrumbCommon from "./breadcrumb";
import {routes} from "./const";
import Sidebar from "./sidebar";
import IpFlowStep from "../components/ip-flow-step/IpFlowStep";
import {connect} from "react-redux";
import {login, loginIndex, logout} from "../store/actions/userAction";

const LayoutMain = ({userState, loginIndex, login, logout}) => {
    const [form] = Form.useForm();

    const onOk = () => {
        form.submit();
    };
    const onFinish = (values) => {
        login(values);
    };

    const logoutUser = () => {
        logout();
        //window.location.href = "/";
    }

    //useEffect
    useEffect(() => {
        loginIndex({});
    }, []);


    if (!userState.token) {
        return (
            <div className="bg-gradient-menu" style={{height: '100%'}}>
                <div className="container">
                    {/* Outer Row */}
                    <div className="row justify-content-center">
                        <div className="col-xl-10 col-lg-12 col-md-9 card o-hidden border-0 shadow-lg my-5">
                            {/* Nested Row within Card Body */}
                            <div className="row">
                                <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                <div className="col-lg-6">
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="h4 text-gray-900 mb-4">Welcome ORC Tools!</h1>
                                        </div>
                                        <Form form={form} layout="vertical" name="loginForm" className="user"
                                              onFinish={onFinish}>
                                            <div className="form-group">
                                                <Form.Item
                                                    name="name"
                                                    rules={[{required: true, message: 'Please input your username!'}]}>
                                                    <Input className="form-control form-control-user"
                                                           id="exampleInputEmail" placeholder="Enter username"/>
                                                </Form.Item>
                                            </div>
                                            <div className="form-group">
                                                <Form.Item
                                                    name="password"
                                                    rules={[{required: true, message: 'Please input your password!'}]}>
                                                    <Input className="form-control form-control-user"
                                                           id="exampleInputPassword" placeholder="Enter password"
                                                           type="password"/>
                                                </Form.Item>
                                            </div>
                                            <div className="form-group">
                                                <Form.Item name="remember" className="custom-checkbox" valuePropName="checked">
                                                    <Checkbox>Remember me</Checkbox>
                                                </Form.Item>
                                            </div>
                                            <button key="submit" className="btn btn-primary btn-user btn-block"
                                                    onClick={onOk}>
                                                Login
                                            </button>
                                        </Form>
                                        <hr></hr>
                                        <div className="text-center">
                                            <a className="small" href="forgot-password.html">Forgot Password?</a>
                                        </div>
                                        <div className="text-center">
                                            <a className="small" href="register.html">Create an Account!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    } else {
        return (
            <>
                <Router>
                    <div id="loading"></div>
                    <div id="wrapper">
                        <Sidebar/>
                        <Layout>
                            <div id="content-wrapper" className="d-flex flex-column">
                                {/* Main Content */}
                                <div id="content" style={{minHeight: "500px"}}>
                                    <HeaderCommon/>
                                    <Switch>
                                        {/*flow step id*/}
                                        <Route exact path="/ip-flow/step/:id" component={IpFlowStep}/>
                                        {/*create testCase*/}
                                        <Route exact path="/testcaseCreate">
                                            <div className="container-fluid">
                                                <BreadcrumbCommon page="Test Case"/>
                                                <ContentCommon id="testcaseCreate"/>
                                            </div>
                                        </Route>
                                        {/*exact route*/}
                                        {routes.map((route, i) => (
                                            <RouteWithSubRoutes key={i} {...route} />
                                        ))}
                                    </Switch>
                                </div>
                                {/* End of Main Content */}
                                <FooterCommon/>
                            </div>
                        </Layout>
                    </div>

                    {/*Scroll to Top Button*/}
                    <a className="scroll-to-top rounded" href="#page-top">
                        <i className="fas fa-angle-up"></i>
                    </a>
                    {/*Logout Modal*/}
                    <div className="modal fade" id="logoutModal" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                    <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div className="modal-body">Select "Logout" below if you are ready to end your current
                                    session.
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel
                                    </button>
                                    <button className="btn btn-primary" onClick={logoutUser} data-dismiss="modal">Logout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Router>
            </>
        );
    }
}

/*A special wrapper for
    <Route> that knows how to
        handle "sub"-routes by passing them in a `routes`
        prop to the component it renders.*/
function RouteWithSubRoutes(route) {
    return (
        <Route exact path={route.path}>
            <div className="container-fluid">
                <BreadcrumbCommon page={route.name}/>
                <ContentCommon id={route.breadcrumb}/>
            </div>
        </Route>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    userState: state.userReducer,
});
//Connect to store
export default connect(mapStateToProps, {loginIndex, login, logout})(LayoutMain);