import React from "react";
import {LOGIN, LOGIN_SUCCESS, LOGOUT} from "../types";
import {openNotificationCommon, startLoading, stopLoading} from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";
import UserService from "../services/userService";

export const loginIndex = () => async (dispatch) => {
    const tokenString = localStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    if (userToken  == undefined ||  userToken == ""){
        dispatch({
            type: LOGIN,
            token: "",
        })
    }else{
        dispatch({
            type: LOGIN_SUCCESS,
            token: userToken,
        })
    }

};

export const login = (params) => async (dispatch) => {
    // startLoading();
    // try {
    //     let data = JSON.stringify({
    //         "data": {
    //             "type": "IpFlow",
    //             "attributes": {"user": params.user, "pass": params.pass}
    //         }
    //     });
    //     const res = await UserService.authenticate(data);
    //     if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
    //         sessionStorage.setItem('token', JSON.stringify("thanh cong"));
    //         openNotificationCommon('success', 'Login User', 'Đăng nhập thành công');
    //         // reset table
    //         dispatch({
    //             type: LOGIN_SUCCESS,
    //             token: "thanh cong",
    //         })
    //     }
    // } catch (err) {
    //     console.log(err);
    //     openNotificationCommon('error', 'Login User', 'Đăng nhập không thành công!');
    // }
    // stopLoading();
    console.log(params)
    openNotificationCommon('success', 'Login User', 'Đăng nhập thành công');
    localStorage.setItem('token', JSON.stringify("thanh cong"));
    dispatch({
        type: LOGIN_SUCCESS,
        token: "thanh cong",
    })

};

export const logout = () => async (dispatch) => {
    // remove user from local storage to log user out
    openNotificationCommon('success', 'Login User', 'Đăng xuất thành công');
    localStorage.clear()
    dispatch({
        type: LOGOUT,
        token: "",
    })
    // window.location.href = "/login";
};
