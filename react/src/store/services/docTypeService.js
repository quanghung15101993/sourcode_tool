import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const getAll = (params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get("/doc-types");
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get("/doc-types",request);
};

const get = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/doc-types/${id}`);
};

const getAllDoctype = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/doc-types?size=1000`);
};


const create = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/doc-types", data);
};

const update = (id, data) => {
    return httpAccess(JSON_API_CONTENT_TYPE).put(`/doc-types/${id}`, data);
};

const remove = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).delete(`/doc-types/${id}`);
};

const getAllRecogProfile = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/reg-profiles?size=1000");
};

const DocTypeService = {
    getAll,
    getAllDoctype,
    get,
    create,
    update,
    remove,
    getAllRecogProfile
};

export default DocTypeService;