import React from "react";
import {Select} from "antd";
import {openNotificationCommon, startLoading, stopLoading} from "../../common/const";
import isNotNullOrUndefined from "../../common/utils";
import DocTypeService from "../services/docTypeService";
import {CREATE_UPDATE_DOC_TYPE, DOC_TYPE, VIEW_DOC_TYPE} from "../types";

const {Option} = Select;

export const docTypeList = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await DocTypeService.getAll(params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let count = repos.data.meta.pagination.count;
            let dataList = repos.data.data;
            let list = [];
            for (let i = 0; i < dataList.length; i++) {
                list.push({
                    key : dataList[i].id,
                    id: dataList[i].id,
                    name: dataList[i].attributes.name,
                    single_fields: dataList[i].attributes.single_fields,
                    table_fields: dataList[i].attributes.table_fields,
                    recog_profile_id: dataList[i].relationships.recog_profile.data.id,
                });
            }
            dispatch({
                type: DOC_TYPE,
                count: count,
                page: params.page,
                payload: list,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

export const createUpdateDocType = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await DocTypeService.getAllRecogProfile();
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let dataRecogProfile = repos.data.data;
            let list = [];
            for (let i = 0; i < dataRecogProfile.length; i++) {
                list.push(
                    <Option value={dataRecogProfile[i].id} key={dataRecogProfile[i].id}>{dataRecogProfile[i].attributes.name}</Option>
                );
            }
            if (params.id == undefined) {
                dispatch({
                    type: CREATE_UPDATE_DOC_TYPE,
                    payload: list,
                    id: "",
                })
            } else {
                const dataRecog = await DocTypeService.get(params.id);
                if (isNotNullOrUndefined(dataRecog) && isNotNullOrUndefined(dataRecog.data)) {
                    let data = dataRecog.data.data;
                    params.form.setFieldsValue({
                        name: data.attributes.name,
                        recogProfile: data.relationships.recog_profile.data.id + "",
                    });
                    dispatch({
                        type: CREATE_UPDATE_DOC_TYPE,
                        payload: list,
                        id: params.id,
                    })
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// detail
export const detailDocType = (params) => async (dispatch) => {
    startLoading();
    try {
        const detail = await DocTypeService.get(params.id);
        if (isNotNullOrUndefined(detail) && isNotNullOrUndefined(detail.data)) {
            let data = detail.data.data.attributes;
            let listSingle = [];
            let listTable = [];
            if(isNotNullOrUndefined(data.single_fields) && data.single_fields !== "" && data.single_fields !== "[]"){
                listSingle = JSON.parse(data.single_fields);
            }
            if(isNotNullOrUndefined(data.table_fields) && data.table_fields !== "" && data.table_fields !== "[]"){
                listTable = JSON.parse(data.table_fields);
            }
            dispatch({
                type: VIEW_DOC_TYPE,
                listSingle: listSingle,
                listTable: listTable,
                id: params.id,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// Insert
export const insertData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "DocType",
                "attributes": {"name": params.name, "recog_profile": params.recogProfile}
            }
        });
        const res = await DocTypeService.create(data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
            // reset table
            const repos = await DocTypeService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        if (isNotNullOrUndefined(err.response)) {
            let message = err.response.data.errors.message;
            openNotificationCommon('error', 'Create bản ghi', message);
        }else{
            openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
        }

    }
    stopLoading();
};

// Update
export const updateData = (params) => async (dispatch) => {
    startLoading();
    try {
        let data = JSON.stringify({
            "data": {
                "type": "DocType",
                "id": params.id,
                "attributes": {
                    "name": params.name,
                    "recog_profile": params.recogProfile,
                }
            }
        });
        const res = await DocTypeService.update(params.id, data);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Update bản ghi', 'Cập nhật thành công bản ghi');
            // reset tables
            const repos = await DocTypeService.getAll({});
            resetTable(repos, dispatch);
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Update bản ghi', 'Cập nhật không thành công bản ghi');
    }
    stopLoading();
};

// Delete
export const deleteData = (params) => async (dispatch) => {
    startLoading();
    try {
        const res = await DocTypeService.remove(params.id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
            // reset tables
            const repos = await DocTypeService.getAll({});
            resetTable(repos, dispatch);
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
    stopLoading();
};

// reset table
const resetTable = (repos, dispatch) => {
    if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
        console.log(repos);
        let count = repos.data.meta.pagination.count;
        let dataList = repos.data.data;
        let list = [];
        for (let i = 0; i < dataList.length; i++) {
            list.push({
                key : dataList[i].id,
                id: dataList[i].id,
                name: dataList[i].attributes.name,
                single_fields: dataList[i].attributes.single_fields,
                table_fields: dataList[i].attributes.table_fields,
                recog_profile_id: dataList[i].relationships.recog_profile.data.id,
            });
        }
        dispatch({
            type: DOC_TYPE,
            count: count,
            page: 1,
            payload: list,
        })

    }
};




