import { combineReducers } from "redux";
import projectReducer from "./projectReducer";
import cmtReducer from "./cmtReducer";
import ipProfileReducer from "./ipProfileReducer";
import recogProfileReducer from "./recogProfileReducer"
import templateReducer from "./templateReducer"
import IpFlowReducer from "./ipFlowReducer";
import IpFlowStepReducer from "./ipFlowStepReducer";
import DocTypeReducer from "./docTypeReducer";
import testCaseReducer from "./testCaseReducer"
import UserReducer from "./userReducer";



const rootReducer = combineReducers({
  cmt: cmtReducer,
  template: templateReducer,
  testCase : testCaseReducer,
  project: projectReducer,
  stateIpProfile: ipProfileReducer,
  recogProfileReducer : recogProfileReducer,
  ipFlowReducer : IpFlowReducer,
  IpFlowStepReducer : IpFlowStepReducer,
  docTypeReducer : DocTypeReducer,
  userReducer : UserReducer,
});

export default rootReducer;
