import React from "react";
import "antd/dist/antd.css";
import {Upload} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import {showImageTestCase,uploadImageProcess,imageProcessTestCase,removeImageTiff} from "../../store/actions/testCaseAction"
import {Button} from 'antd';
import {connect} from "react-redux";
import isNotNullOrUndefined from "../../common/utils";
import $ from "jquery";
import Tiff from "tiff.js";

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const TestCaseAttachFile = ({testCaseState, showImageTestCase,uploadImageProcess,imageProcessTestCase,removeImageTiff}) => {

    
    const handlePreview = async (file,fileList) => {
        if (file!=undefined) {
            if (!file.url && !file.preview) {
                file.preview = await getBase64(file.originFileObj);
            }
            await showImageTestCase(file, fileList)
        }
    };
    
    const handleChange = async (fileList) => {
        
        let dataImage = $("#original_img_prev_tif")
        if (isNotNullOrUndefined(fileList) && isNotNullOrUndefined(fileList.fileList[0])) {
                var reader = new FileReader();
                    reader.onload = function (e) {
                        // get loaded data and render thumbnail, file size max 100Mb.
                        Tiff.initialize({
                            TOTAL_MEMORY: 100000000
                        });
 
                        var tiff = new Tiff({
                            buffer: e.target.result
                        });
            
                        var tiffCanvas = tiff.toCanvas();
                        $(tiffCanvas).css({
                            "max-width": "60%",
                            "width": "100%",
                            "height": "auto",
                            "display": "block",
                            "padding-top": "2%"
                        }).addClass("preview-tif").attr('id', 'original_img_prev_tif');
                         $('#original-image-tif').append(tiffCanvas); //btn-upload-image
                        handlePreview(fileList.fileList[0], fileList.fileList)
                    };
                    reader.readAsArrayBuffer(fileList.fileList[0].originFileObj); 
        }else if(isNotNullOrUndefined(dataImage[0])){
            $("#original_img_prev_tif").remove()
            $("#result_img_prev_tif").remove()
            $("#canvas-input-data-tif").remove()
            removeImageTiff()
        }
    };

    const uploadImage = async(e) =>{
        console.log(uploadImage);
        let fileContent = e[0].preview.replace("data:image/tiff;base64,","")
        
        let imageName = e[0].name
        let bodyImage = JSON.stringify({
            "data": {
                "type": "ImageUploadApiView",
                "attributes": {
                    "name": imageName,
                    "file_content": fileContent,
                }
            }
        })
        await uploadImageProcess(bodyImage, testCaseState.projectFlowId, testCaseState.flowProjectId, testCaseState.description)       
    }

    const processImage =async()=>{
        imageProcessTestCase(testCaseState.testCaseId)
    }

    const uploadButton = (
        <div>
            <PlusOutlined/>
            <div>Browse</div>
        </div>)
    return (
        <>
        
            <Upload
                action=""
                listType="picture-card"
                disabled = {testCaseState.disabledBrowse}
                onChange={(e)=>handleChange(e)}
                beforeUpload={() => false}
            >
                {testCaseState.fileList.length >= 2 ? null : uploadButton}
            </Upload>
            <div className="row">
                <div className="col-md-6">
               
                    <div style={{marginLeft:'35%'}}>
                        <Button  disabled = {testCaseState.disabledUpload} type="primary" onClick={(e) => uploadImage(testCaseState.browseUpload)}>
                              Upload
                        </Button>
                    </div>
                    <br />
                    <div>
                        <div id="original-image-tif" align="center">
                        </div>
                    </div>     
                </div>
                <div>
                </div>
                <div className="col-md-6">
                    <div style={{marginLeft:'35%'}}>
                            <Button  disabled = {testCaseState.disabledProcessImage} type="primary" onClick={() => processImage()}>
                                Process
                            </Button>
                    </div>
                    <br />
                    <div id ="#processed-image-tif">
                    </div>
                </div>
            </div>
            <div>
            </div>
        </>
    );
    // }
}
//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});
//Connect to store
export default connect(mapStateToProps, {showImageTestCase,uploadImageProcess,imageProcessTestCase,removeImageTiff})(TestCaseAttachFile);
