import {CREATE_UPDATE_IP_FLOW_STEP, IP_FLOW_STEP} from "../types";

const initialState = {
    idRecogProfile : "",
    listIPProject: [],
    listIpFlowStep: [],
    total: 10,
    current: 1,
};

const IpFlowStepReducer = (state = initialState, action) => {
    switch (action.type) {
        case IP_FLOW_STEP:
            return {
                ...state,
                listIpFlowStep : action.payload,
                current: action.page,
                total: action.count
            };
        case CREATE_UPDATE_IP_FLOW_STEP:
            return {
                ...state,
                listIPProject : action.payload,
            };

        default:
            return state;
    }
};

export default IpFlowStepReducer;
