from decouple import config
import motor.motor_asyncio

MONGO_DETAILS = config('MONGO_DETAILS')
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
database = client[config('DATABASE_NAME')]


def get_collection(name):
    return database.get_collection(name)
