import {Form, Button} from "antd";
import React from "react";
import {connect} from "react-redux";
import {insertAssertData} from "../../store/actions/testCaseAction";
import $ from "jquery"


function TestCaseInputData({testCaseState,insertAssertData}) {

    const [form] = Form.useForm();

    const onFinish = async (dataInsert) => { 

        let listData= []
        for (var k in dataInsert) {
            if (dataInsert[k] === undefined) {
                listData.push("")
                continue;
            }
            listData.push(dataInsert[k])
        }

        let listExtractExpect = []   
        let data = ""     
        let singleFields = testCaseState.singleFields        
        singleFields.map((singleField,index)=>{
            Object.keys(singleField).forEach((key) => { 
                if (key === "name") {
                    data = singleField[key];
                }
                if (key ==="value") {
                    listExtractExpect.push({
                        "name": data,
                        "type": "String",
                        "value": listData[index],
                    }) 
                }
                 
            })
        })
        let body = JSON.stringify({
            "data": {
                 "type": "AssertData",
                 "attributes": {
                    "test_case": testCaseState.testCaseId,
                    "recog_profile_id": testCaseState.recogId,
                    "doctype_id": testCaseState.doctypeId,
                    "template_id":testCaseState.templateId, 
                    "classify_expect": {
                        "doc_type_name":testCaseState.doctypeName,
                        "captiva": testCaseState.templateId
                    },
                    "extract_expect": listExtractExpect// accept json value
                }
            }
        })
        console.log(body);
        insertAssertData(body)       
    };
    
    return (
        <>
            <Form form={form} layout="vertical" name="testCaseData" onFinish={onFinish}>
            <div style={{display: `${testCaseState.displayAssertDataTag}`}} className= "bt-center">    
                <Form.Item>
                    <Button type="primary" htmlType="submit" disabled ={testCaseState.dislayAssertDataBt}>Assert Data</Button>
                </Form.Item>
            </div>
            
                {testCaseState.listData}
           
            </Form>
        </>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
    testCaseState: state.testCase,
});

export default connect(mapStateToProps, {insertAssertData})(TestCaseInputData);

