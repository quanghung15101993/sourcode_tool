import { GET_PROJECT,OPEN_POP_PROJECT,CLOSE_POP_PROJECT,INSERT_PROJECT } from "../types";
import {openNotificationCommon} from "../../common/const"
import ProjectService from "../services/projectService";
import isNotNullOrUndefined from "../../common/utils";

export const getProject = (params = {}) => async (dispatch) => {
  
  try {
      // get response from api
      const repos = await ProjectService.getAll(params);
      if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
        let countData = repos.data;
        let list = [];
        let data = countData.data;
        let lenght = data.length
        let count = countData.meta.pagination.count;
        for (let i = 0; i < lenght; i++) {
            
            list.push({
                id:data[i].id,
                name: data[i].attributes.name,
                description: data[i].attributes.description,
            });
        }
        dispatch({
          type: GET_PROJECT,
          // it can write ...count
          count: count,
          page: params.page,
          payload: list,
          
      });
      }
  } catch (error) {
    console.log(error);
  }
};

export const openPop = (param) => async (dispatch) => {
  try {
      
    if (param === "create") {
        dispatch({
            type: OPEN_POP_PROJECT,
            value: param
        });   
      }else{
            // get response from api
            const repos = await ProjectService.get(param);
            dispatch({
                type: OPEN_POP_PROJECT,
                payload: repos,
            });       
        };
  } catch (error) {
    console.log(error);
  } 
}

export const closePop = () => async (dispatch) => {
    try {
        dispatch({
            type: CLOSE_POP_PROJECT,
        });
    } catch (error) {
        console.log(error);
    }
};

//Delete
export const deleteProject = (id) => async (dispatch) => {
  
    try {
        const res = await ProjectService.remove(id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }

};

// Insert
export  const insertData =(body) => async (dispatch) => {
    try {
        const res = await ProjectService.create(body);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            dispatch({
                type: INSERT_PROJECT,
            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
  };

// Update
export const updateProject = (body,id) => async (dispatch) => {
    try {
        const res = await ProjectService.update(id,body);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
};
