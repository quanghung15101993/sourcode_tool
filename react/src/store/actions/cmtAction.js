import {CLOSE_POPUP_CMT, POST_CMT, UPLOAD_CMT} from "../types";
import axios from "axios"
import  {REST_API_POST_CMT} from "../../common/const"
import {startLoading, stopLoading} from "../../common/const"
//this example handle close popup but present not use
export const closePop = () => async (dispatch) => {
    startLoading()
    try {
        dispatch({
            type: CLOSE_POPUP_CMT,
            payload: false,
        });
    } catch (error) {
        console.log(error);
    }
    stopLoading()
};


export const uploadCMT = (fileList) => async (dispatch) => {  
    console.log("1");
    try {
        dispatch({
            type: UPLOAD_CMT,
            payload: fileList,
        });
    } catch (error) {
        console.log(error);
    }
};

export const postImage = (bodyImage, file) => async (dispatch) => {
    startLoading()
    try {
        console.log("start");
        await axios.post(REST_API_POST_CMT, bodyImage,{
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            console.log("action", response);
            dispatch({
                type: POST_CMT,
                payload: response.data,
                // it can write ...file
                file: file,
            });
        })
    } catch (error) {
        console.log(error);
    }
    stopLoading()
};

