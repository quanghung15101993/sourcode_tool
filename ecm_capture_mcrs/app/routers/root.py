from fastapi import FastAPI

from fastapi.openapi.utils import get_openapi

from app.core.middlewares.exception_handler import BusinessException, error_handle_common
from app.routers.batch import batch_router
from app.routers.file_upload_api import batch_upload_router

app = FastAPI()

CAPTURE_MCRS_PREFIX = "/capture-mcrs/v1"


class AppInit:

    def __init__(self, title, data, path):
        openapi_schema = get_openapi(
            title=title,
            version="1.0",
            description="Capture microservice.",
            routes=app.routes,
        )
        openapi_schema["info"]["data"] = data
        self.data = openapi_schema


@app.get("/", tags=["capture microservice"])
async def capture_mcrs_init():
    data = {"name": "capture-mcrs"}
    return AppInit("tit", data, "/")


app.include_router(batch_router, tags=["batch information"], prefix=CAPTURE_MCRS_PREFIX)
app.include_router(batch_upload_router, tags=["batch file upload"], prefix=CAPTURE_MCRS_PREFIX)
app.add_exception_handler(BusinessException, error_handle_common)
