import {Button, Form, InputNumber, Modal, Select} from "antd";
import React, {useState} from "react";
import {connect} from "react-redux";
import {createUpdateIpFlowStep, insertData, updateData} from "../../store/actions/ipFlowStepAction";

const IpFlowStepForm = ({ipFlowState, idFlow, createUpdateIpFlowStep, insertData, updateData}) => {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const showModalCreate = () => {
        form.resetFields();
        createUpdateIpFlowStep({id: idFlow.id, idFlow: idFlow.idFlow, form: form});
        setIsModalVisible(true);
    }
    const showModalEdit = () => {
        form.resetFields();
        createUpdateIpFlowStep({id: idFlow.id, idFlow: idFlow.idFlow, form: form});
        setIsModalVisible(true);
    }

    const onOk = () => {
        form.submit();
    };

    const onFinish = (values) => {
        if (idFlow.id === "") {
            insertData(idFlow.idFlow, values)
        } else {
            updateData(idFlow.idFlow,{
                id : idFlow.id,
                ip_profile_id: values.profile,
                step: values.step
            });
        }
        setIsModalVisible(false);

    };

    if (idFlow.id === "") {
        return (
            <>
                <Button type="primary" onClick={showModalCreate} key="create" align="end" style={{"float": "right"}}>
                    Create Flow Step
                </Button>
                <Modal title="Create Flow Step"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Create
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="profile"
                            label="Profile:"
                            rules={[{required: true, message: 'Please choose a image process profile!'}]}>
                            <Select
                                placeholder="Select a Project for IP Flow"
                                allowClear>
                                {ipFlowState.listIPProject}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="step"
                            label="Step:"
                            rules={[{required: true, message: 'Please input Flow step no!'}]}>
                            <InputNumber min={1}></InputNumber>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    } else {
        return (
            <>
                <Button type="primary" onClick={() => showModalEdit()}>
                    Edit
                </Button>
                <Modal title="Edit Flow Step"
                       visible={isModalVisible}
                       onOk={onOk}
                       onCancel={handleCancel}
                       footer={[
                           <Button key="submit" type="primary" onClick={onOk}>
                               Edit
                           </Button>,
                           <Button key="back" danger onClick={handleCancel}>
                               Cancel
                           </Button>,
                       ]}>
                    <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                        <Form.Item
                            name="profile"
                            label="Profile:"
                            rules={[{required: true, message: 'Please choose a image process profile!'}]}>
                            <Select
                                placeholder="Select a Project for IP Flow"
                                allowClear>
                                {ipFlowState.listIPProject}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="step"
                            label="Step:"
                            rules={[{required: true, message: 'Please input Flow step no!'}]}>
                            <InputNumber min={1}></InputNumber>
                        </Form.Item>
                    </Form>
                </Modal>
            </>
        );
    }

}

//Get state from Store
const mapStateToProps = (state, ownProps) => ({
    ipFlowState: state.IpFlowStepReducer,
    idFlow: ownProps,
});
//Connect to store
export default connect(mapStateToProps, {createUpdateIpFlowStep, insertData, updateData})(IpFlowStepForm);