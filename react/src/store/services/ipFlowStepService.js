import {httpAccess, JSON_API_CONTENT_TYPE} from "../../common/http-commons";
import isNotNullOrUndefined from "../../common/utils";

const getAll = (idFlow, params) => {
    let parameters = new URLSearchParams();
    if (!isNotNullOrUndefined(params)) {
        return httpAccess(JSON_API_CONTENT_TYPE).get(`/ip-flows-v/${idFlow}/steps`);
    }
    if (isNotNullOrUndefined(params.page)) {
        parameters.append("page",params.page)
    }
    if (isNotNullOrUndefined(params.sort)) {
        parameters.append("sort",params.sort)
    }
    if (isNotNullOrUndefined(params.search)) {
        parameters.append("filter[search]",params.search)
    }
    let request = {
        params: parameters
    };
    return httpAccess(JSON_API_CONTENT_TYPE).get(`/ip-flows-v/${idFlow}/steps`,request);
};

const get = (idFlow, idStep) => {
    return httpAccess(JSON_API_CONTENT_TYPE).get(`ip-flows-v/${idFlow}/steps/${idStep}`);
};

const create = data => {
    return httpAccess(JSON_API_CONTENT_TYPE).post("/ip-flows/steps", data);
};

const update = (id, data) => {
    return httpAccess(JSON_API_CONTENT_TYPE).put(`/ip-flows/steps/${id}`, data);
};

const remove = id => {
    return httpAccess(JSON_API_CONTENT_TYPE).delete(`ip-flows/steps/${id}`);
};

const getAllIPProject = () => {
    return httpAccess(JSON_API_CONTENT_TYPE).get("/ip-profiles?size=1000");
};


const IpFlowStepService = {
    getAll,
    get,
    create,
    update,
    remove,
    getAllIPProject
};

export default IpFlowStepService;