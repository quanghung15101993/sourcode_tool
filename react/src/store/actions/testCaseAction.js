import {
    GET_TEST_CASE,
    OPEN_POP_TEST_CASE,
    LOAD_RECOG_DOCTYPE_TEST_CASE,
    CLOSE_POP_TEST_CASE,
    LOAD_PROJECT_TEST_CASE,
    PROCESS_IMAGE_TEST_CASE,
    GET_DESCRIP_VALUE,
    LOAD_PROJECT_FLOW_RECOG_TEST_CASE,
    LOAD_TEMPLATE_TEST_CASE,
    LOAD_FLOW_TEST_CASE,
    UPLOAD_IMAGE_TEST_CASE,
    RUN_TEST_CASE,
    SHOW_IMAGE_TEST_CASE,
    REMOVE_IMAGE_TIFF,
    LOAD_DOCTYPE_TEMPLATE_TEST_CASE,
    INSERT_ASSERT_DATA,
    GET_TEST_CASE_ALL_VIEW, GET_TEST_CASE_DETAIL_VIEW
} from "../types";
import {openNotificationCommon, startLoading, stopLoading} from "../../common/const"
import TestCaseService from "../services/testCaseService";
import ProjectService from "../services/projectService";
import DocTypeService from "../services/docTypeService";
import isNotNullOrUndefined from "../../common/utils";
import {Select} from "antd";
import $ from "jquery";
import Tiff from "tiff.js";

const {Option} = Select;

// get list testcase
export const getTestCaseList = (params) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await TestCaseService.getAllTestCaseV(params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let count = repos.data.meta.pagination.count;
            let dataList = repos.data.data;
            let listTestCase = [];
            for (let i = 0; i < dataList.length; i++) {
                listTestCase.push({
                    key: dataList[i].id,
                    tc_id: dataList[i].attributes.tc_id,
                    tc_description: dataList[i].attributes.tc_description,
                    project_name: dataList[i].attributes.project_name,
                    recog_profile_name: dataList[i].attributes.recog_profile_name,
                    doctype_name: dataList[i].attributes.doctype_name,
                    template_name: dataList[i].attributes.template_name,
                    image_result_name: dataList[i].attributes.image_result_name,
                    captiva_model_id: dataList[i].attributes.captiva_model_id,
                });
            }
            dispatch({
                type: GET_TEST_CASE_ALL_VIEW,
                count: count,
                page: params.page,
                payload: listTestCase,
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

// get testcase detail
export const getTestCaseDetail = (params) => async (dispatch) => {
    startLoading();
    try {
        let dataResult = [];
        let listTable = [];
        // get response from api
        const repos = await TestCaseService.getTestCaseV(params);
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            dataResult = repos.data.data.attributes;
            listTable = [];
            let listTestResults = null;
            let listExtractExpect = JSON.parse(repos.data.data.attributes.extract_expect);
            if(repos.data.data.attributes.test_results != "" && isNotNullOrUndefined(repos.data.data.attributes.test_results)){
                listTestResults = JSON.parse(repos.data.data.attributes.test_results[0].extract_result);
            }

            if (isNotNullOrUndefined(listExtractExpect) && isNotNullOrUndefined(listTestResults)) {
                if (listExtractExpect.length === listTestResults.length) {
                    for (let i = 0; i < listExtractExpect.length; i++) {
                        listTable.push({
                            key: i,
                            id: (i + 1) + "",
                            extract_expect_name: listExtractExpect[i].name,
                            extract_expect_value: listExtractExpect[i].value,
                            extract_result_name: listTestResults[i].name,
                            extract_result_value: listTestResults[i].value,
                            result_compare: ""
                        });
                    }
                }
            }else if(isNotNullOrUndefined(listExtractExpect) && !isNotNullOrUndefined(listTestResults)) {
                for (let i = 0; i < listExtractExpect.length; i++) {
                    listTable.push({
                        key: i,
                        id: (i + 1) + "",
                        extract_expect_name: listExtractExpect[i].name,
                        extract_expect_value: listExtractExpect[i].value,
                        extract_result_name: "",
                        extract_result_value: "",
                        result_compare: ""
                    });
                }

            }else if (!isNotNullOrUndefined(listExtractExpect) && isNotNullOrUndefined(listTestResults)) {
                for (let i = 0; i < listTestResults.length; i++) {
                    listTable.push({
                        key: i,
                        id: (i + 1) + "",
                        extract_expect_name: "",
                        extract_expect_value: "",
                        extract_result_name: listTestResults[i].name,
                        extract_result_value: listTestResults[i].value,
                        result_compare: ""
                    });
                }
            }
        }
        
        // build original_image_content
        if (isNotNullOrUndefined(repos.data.data.attributes.original_image_content)) {
            Tiff.initialize({
                TOTAL_MEMORY: 1000000000
            });
            var imageArrBuffer = base64StringToArray(repos.data.data.attributes.original_image_content)
            var tiff = new Tiff({
                buffer: imageArrBuffer
            })
            var tiffCanvas = tiff.toCanvas();
            $(tiffCanvas).css({
                "max-width": "60%",
                "width": "100%",
                "height": "auto",
                "display": "block",
                "padding-top": "2%"
            }).attr('id', "result_img_prev_tif");
            const myNode = document.getElementById(params+"").children[0]
            //const myNode = document.getElementById("#original_image_content");
            while (myNode.hasChildNodes()) {
                myNode.removeChild(myNode.lastChild);
            }
            myNode.append(tiffCanvas);
        }

        // // build image_result_content
        // if (isNotNullOrUndefined(repos.data.data.attributes.image_result_content)) {
        //     Tiff.initialize({
        //         TOTAL_MEMORY: 1000000000
        //     });
        //     var imageResult = base64StringToArray(repos.data.data.attributes.image_result_content)
        //     var tiff2 = new Tiff({
        //         buffer: imageResult
        //     })
        //     var tiffCanvas2 = tiff2.toCanvas();
        //     $(tiffCanvas2).css({
        //         "max-width": "60%",
        //         "width": "100%",
        //         "height": "auto",
        //         "display": "block",
        //         "padding-top": "2%"
        //     })
        //     const myNode = document.getElementById("#image_result_content");
        //     while (myNode.hasChildNodes()) {
        //         myNode.removeChild(myNode.lastChild);
        //     }
        //     document.getElementById('#image_result_content').append(tiffCanvas2);
        // }
        // table extract vs
        dispatch({
            type: GET_TEST_CASE_DETAIL_VIEW,
            payload: dataResult,
            listTable: listTable,
        })
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

//Get Project data to fill into the Combobox
export const getProject = () => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await ProjectService.getAllProject();
        // Get data list from response
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let data = repos.data.data;
            dispatch({
                type: LOAD_PROJECT_TEST_CASE,
                data: data
            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

//Option Project value to get FLow and Recog data
export const projectOptinon = (projectId) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const reposFlow = await TestCaseService.getProjectFlow(projectId);
        // Get data list from response
        if (isNotNullOrUndefined(reposFlow) && isNotNullOrUndefined(reposFlow.data)) {
            // get response from api
            let dataFlow = reposFlow.data.data;
            const reposRecog = await TestCaseService.getProjectRecog(projectId);
            if (isNotNullOrUndefined(reposRecog) && isNotNullOrUndefined(reposRecog.data)) {
                var dataRecog = reposRecog.data.data
            }
            dispatch({
                type: LOAD_PROJECT_FLOW_RECOG_TEST_CASE,
                dataFlow: dataFlow,
                dataRecog: dataRecog,
                projectId: projectId,

            })
        }
    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

//Option Flow value to display screen Upload module
export const flowOption = (flowId) => async (dispatch) => {
    startLoading();
    try {
        dispatch({
            type: LOAD_FLOW_TEST_CASE,
            flowId: flowId,
        })

    } catch (err) {
        console.log(err);
    }
    stopLoading();
};
//Option RecoProfile value to get Doctype data
export const recogDoctypeOption = (recogId) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await TestCaseService.getRecogDocType(recogId);
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let data = repos.data.data;
            dispatch({
                type: LOAD_RECOG_DOCTYPE_TEST_CASE,
                data: data,
                recogId: recogId,
            })
        }

    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

//Option Doctype value to get Template data
export const doctypeTemplateOption = (doctypeId, doctypeName) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await TestCaseService.getDocTypeTemplate(doctypeId);
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {

            let data = repos.data.data;
            dispatch({
                type: LOAD_DOCTYPE_TEMPLATE_TEST_CASE,
                data: data,
                doctypeName: doctypeName,
                doctypeId: doctypeId,
            })
        }

    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

//Option Template value after screen display will automatically generated content
export const templateOption = (templateId, doctypeId, imageTiifCanvas) => async (dispatch) => {
    startLoading();
    try {
        const repos = await DocTypeService.get(doctypeId);
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let singleFields = repos.data.data.attributes.single_fields
            var data = JSON.parse(singleFields);
            dispatch({
                type: LOAD_TEMPLATE_TEST_CASE,
                singleFields: data,
                imageTiifCanvas: imageTiifCanvas,
                templateId: templateId,
            })
        }


    } catch (err) {
        console.log(err);
    }
    stopLoading();
};

export const getTestCase = (params = {}) => async (dispatch) => {
    startLoading();
    try {
        // get response from api
        const repos = await TestCaseService.getAll(params);
        if (isNotNullOrUndefined(repos) && isNotNullOrUndefined(repos.data)) {
            let countData = repos.data;
            let list = [];
            let data = countData.data;
            let lenght = data.length
            let count = countData.meta.pagination.count;
            for (let i = 0; i < lenght; i++) {

                list.push({
                    id: data[i].id,
                    name: data[i].attributes.name,
                    docTypeId: data[i].attributes.doc_type_id,
                    docTypeName: data[i].attributes.doc_type_name,
                    captivaId: data[i].attributes.captiva_id,
                });
            }
            dispatch({
                type: GET_TEST_CASE,
                // it can write ...count
                count: count,
                page: params.page,
                payload: list,

            });
        }
    } catch (error) {
        console.log(error);
    }

    stopLoading();
};

export const openPop = (param) => async (dispatch) => {
    startLoading();
    try {
        if (param === "create") {
            dispatch({
                type: OPEN_POP_TEST_CASE,
                value: param
            });
        } else {
            // get response from api
            const repos = await TestCaseService.get(param);
            dispatch({
                type: OPEN_POP_TEST_CASE,
                payload: repos,
            });
        }

    } catch (error) {
        console.log(error);
    }
    stopLoading();
}

export const closePop = () => async (dispatch) => {
    try {
        dispatch({
            type: CLOSE_POP_TEST_CASE,
        });
    } catch (error) {
        console.log(error);
    }
};

//Delete
export const deleteTestCase = (id) => async (dispatch) => {
    startLoading();
    try {
        const res = await TestCaseService.remove(id);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data) && res.status == 204) {
            openNotificationCommon('success', 'Remove bản ghi', 'Xóa thành công bản ghi');
        } else {
            openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Remove bản ghi', 'Xóa không thành công bản ghi');
    }
    stopLoading();
};

// Insert
export const imageProcessTestCase = (testCaseId) => async (dispatch) => {
    startLoading();
    try {
        console.log(testCaseId);
        const res = await TestCaseService.imageProcessTestCase(testCaseId);

        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            console.log(res);
            Tiff.initialize({
                TOTAL_MEMORY: 10000000
            });
            // build tiff object
            var imageArrBuffer = base64StringToArray(res.data.data.image_content_result)
            var tiff = new Tiff({
                buffer: imageArrBuffer
            })

            var tiffCanvas = tiff.toCanvas();
            $(tiffCanvas).css({
                "max-width": "60%",
                "width": "100%",
                "height": "auto",
                "display": "block",
                "padding-top": "2%"
            }).addClass("preview-tif").attr('id', "result_img_prev_tif");
            $('#processed-image-tif').append(tiffCanvas);
            document.getElementById('#processed-image-tif').append(tiffCanvas);
            // let data = res.data.data.image_content_result
            dispatch({
                type: PROCESS_IMAGE_TEST_CASE,
                // payload: data,
                // tag:tiffCanvas
            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

function base64StringToArray(s) {
    let asciiString = atob(s);
    return new Uint8Array([...asciiString].map(char => char.charCodeAt(0)));
}

export const insertAssertData = (body) => async (dispatch) => {
    startLoading();
    try {
        const res = await TestCaseService.postAsserData(body);

        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {

            console.log(res.data.data);
            dispatch({
                type: INSERT_ASSERT_DATA,
            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

// Update
export const updateTestCase = (body, id) => async (dispatch) => {
    startLoading();
    try {
        const res = await TestCaseService.update(id, body);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

export const removeImageTiff = () => async (dispatch) => {
    startLoading();
    try {
        dispatch({
            type: REMOVE_IMAGE_TIFF,
        });
    } catch (error) {
        console.log(error);
    }
    stopLoading();
};

export const runTestCase = (testCaseId) => async (dispatch) => {
    startLoading();
    try {
        const res = await TestCaseService.runTestCase(testCaseId);
        console.log(res);
        if (isNotNullOrUndefined(res) && isNotNullOrUndefined(res.data)) {
            let extractResult = res.data.data.attributes.extract_result
            var data = JSON.parse(extractResult);
            dispatch({
                type: RUN_TEST_CASE,
                extractResult: data,
            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

export const showImageTestCase = (file, fileList) => async (dispatch) => {
    startLoading();
    try {
        dispatch({
            type: SHOW_IMAGE_TEST_CASE,
            payload: fileList,
            file: file
        })
    } catch (error) {
        console.log(error);
    }
    stopLoading();
};

export const uploadImageProcess = (body, projectId, flowId, description) => async (dispatch) => {
    startLoading();
    try {
        const res = await TestCaseService.uploadImageProcess(body);
        console.log(projectId, flowId);

        let iamgeId = res.data.data.image_id

        let bodyTestCase = JSON.stringify({
            "data": {
                "type": "TestcaseApiView",
                "attributes": {
                    "project_id": projectId,
                    "image_id": iamgeId,
                    "ip_flow_id": flowId,
                    "tags": description,
                }
            }
        })
        console.log(bodyTestCase);
        const resTestCase = await TestCaseService.create(bodyTestCase);
        if (isNotNullOrUndefined(resTestCase) && isNotNullOrUndefined(resTestCase)) {
            let testCaseId = resTestCase.data.data.testcase_id
            dispatch({
                type: UPLOAD_IMAGE_TEST_CASE,
                imageId: iamgeId,
                testCaseId: testCaseId

            });
            openNotificationCommon('success', 'Create bản ghi', 'Thêm mới thành công bản ghi');
        }
    } catch (err) {
        console.log(err);
        openNotificationCommon('error', 'Create bản ghi', 'Thêm mới không thành công bản ghi');
    }
    stopLoading();
};

export const getDescripValue = (value) => async (dispatch) => {

    dispatch({
        type: GET_DESCRIP_VALUE,
        value: value,
    });

}