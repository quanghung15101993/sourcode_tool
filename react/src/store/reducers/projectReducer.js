import {GET_PROJECT,OPEN_POP_PROJECT,CLOSE_POP_PROJECT,INSERT_PROJECT} from "../types";

const initialState = {
    dataSource: [],
    loading: false,
    total: 10,
    current: 1,
    fileList: [],
    isModalVisibleCreate: false,
    listProjectSource:[],
    name:"",
    description:"",
};

const projectReducer = (state = initialState, action) => {
    //this example handle close popup but present not use
    switch (action.type) {
        case GET_PROJECT:
            console.log("reducer");
            //Check page undefined
            if (action.page) {
                return {
                    ...state,
                    current: action.page,
                    dataSource: action.payload,
                    total: action.count
                };
            }
            return {
                ...state,  
                dataSource: action.payload,
                loading: false,
                total: action.count
            };
            case OPEN_POP_PROJECT:
                if (action.value ==="create") {
                    return{
                        ...state,
                        isModalVisibleCreate: true,
                    };
                }
                let data = action.payload.data.data.attributes
                return {
                    ...state,
                    name: data.name,
                    description: data.description,
                };  
                case INSERT_PROJECT:
                    console.log("insert reducer");
                    return {
                        ...state,      
                        isModalVisibleCreate: false,
                    };
                case CLOSE_POP_PROJECT:
                    return {
                        ...state,
                        isModalVisibleCreate:false,
                    };      
        default:
            return state;
    }
};

export default projectReducer;
